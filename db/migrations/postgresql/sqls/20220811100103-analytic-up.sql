CREATE SCHEMA IF NOT EXISTS analytic;

-- analytic.camea_bikecounters_in_contract definition

CREATE TABLE IF NOT EXISTS analytic.camea_bikecounters_in_contract (
	id int4 NULL,
	location_id varchar(50) NULL,
	location_name varchar(50) NULL,
	route_name varchar(50) NULL,
	lat numeric NULL,
	lng numeric NULL,
	in_camea_contract bool NULL,
	old_id varchar(50) NULL
);

INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(32, 'BC_KA-KMML', 'Kampa', 'A1', 50.083, 14.408, true, NULL);
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(1, 'BC_AT-STLA', 'Atletická', 'A140', 50.079, 14.379, true, NULL);
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(2, 'BC_DE-EVSA', 'Divoká Šárka', 'A 33', 50.094, 14.321, false, NULL);
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(3, 'BC_PM-MHJA', 'Pelléova', 'A 160', 50.098, 14.407, true, 'spejchar-pelleova');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(4, 'BC_KO-VYHO', 'Kolčavka', 'A 26', 50.11, 14.495, false, NULL);
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(5, 'BC_HZ-CE', 'Hlubočepská', 'A 12', 50.04, 14.404, true, 'hlubocepska');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(6, 'BC_SU-VS', 'V Šáreckém údolí', 'A 17', 50.119, 14.383, false, NULL);
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(7, 'BC_DU-KRST', 'Dubeč', 'A 24', 50.06, 14.594, false, NULL);
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(8, 'BC_MB-PRVI', 'Mladoboleslavská', 'A267', 50.126, 14.535, true, NULL);
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(9, 'BC_VF-ARUE', 'Stezka okolo Rokytky', 'A 26', 50.105, 14.503, true, 'vysocany');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(10, 'BC_CL-PVLI', 'U Českých loděnic', 'A2', 50.114, 14.458, true, NULL);
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(11, 'BC_ST-RABA', 'Lahovičky (Strakonická)', 'A 1', 49.995, 14.398, true, 'lahovicky-strakonicka');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(12, 'BC_NM-CEKV', 'Nuselský most', 'A 41', 50.068, 14.43, true, 'nuselsky-most');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(13, 'BC_CB-CHTU', 'Chodov', 'A 22', 50.031, 14.493, true, 'chodov');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(14, 'BC_JG-PNPC', 'Jeremenkova', 'A 221', 50.044, 14.423, true, 'podoli');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(15, 'BC_BS-BMZL', 'Barrandovský most', 'A 12', 50.042, 14.407, true, 'barrandovsky-most-x-strakonicka');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(16, 'BC_SN-NDKM', 'Smetanovo nábřeží', 'A2', 50.084, 14.413, true, NULL);
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(17, 'BC_VS-CE', 'Košíře', 'A 14', 50.07, 14.386, false, 'kosire');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(18, 'BC_SH-SEKU', 'Šeberov', 'A 21', 50.012, 14.501, false, 'seberov');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(19, 'BC_KJ-HRHO', 'Nábřeží Kapitána Jaroše', 'A 1', 50.097, 14.434, true, NULL);
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(20, 'BC_NS-PAVL', 'Střešovice', 'A 165X', 50.093, 14.371, false, 'stresovice');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(21, 'BC_HY-WINP', 'Hybernská', 'A25', 50.087, 14.434, true, NULL);
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(22, 'BC_VR-ST', 'Vršovická', 'A 23', 50.066, 14.447, true, 'vrsovicka');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(23, 'BC_VK-MOKO', 'Modřany', 'A 2', 50.003, 14.403, true, 'modrany');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(24, 'BC_PN-VYBR2', 'Podolské nábřeží - vozovka', 'A 2', 50.062, 14.418, true, 'podolske-nabrezi-vozovka');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(25, '100047647', 'Praha - Podolí', NULL, 50.059, 14.419, false, NULL);
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(26, 'BC_EU-KTOT', 'Elsnicovo náměstí x U Českých loděnic', 'A 2', 50.107, 14.471, false, 'elsnicovo-namesti-x-u-ceskych-lodenic');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(27, 'BC_PN-VYBR', 'Podolské nábřeží - stezka', 'A 2', 50.062, 14.418, true, 'podolske-nabrezi-stezka');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(28, 'BC_VK-HRUP', 'Drážní stezka - Vítkov', 'A 25', 50.089, 14.461, true, 'vitkov');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(29, 'BC_CT-OTPB', 'Celetná', 'A 1', 50.087, 14.426, false, 'celetna');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(30, 'BC_RN-JK', 'Rohanské nábřeží', 'A 2', 50.094, 14.438, true, 'rohanske-nabrezi');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(31, 'BC_KR-RAZB', 'Radotín', 'A 11', 49.98, 14.363, true, 'radotin');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(33, 'BC_AL-STPL', 'Anděl (Plzeňská)', 'A14', 50.072, 14.399, true, NULL);
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(34, 'BC_SU-KRKA', 'Krč (Sulická)', 'A 22', 50.035, 14.444, true, 'krc-sulicka');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(35, 'BC_PT-ZOVO', 'Povltavská', 'A 2', 50.115, 14.414, true, 'povltavska');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(36, 'BC_ZA-KLBO', 'V Zámcích', 'A2', 50.143, 14.399, true, NULL);
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(37, 'BC_PP-ROJP', 'Podbabská', 'A 1', 50.117, 14.392, true, 'podbabska');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(38, 'BC_DS-KJVL', 'Dukelských hrdinů', 'A 310', 50.097, 14.433, true, 'dukelskych-hrdinu');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(39, 'BC_TL-TRHO', 'Císařský ostrov', 'A 310', 50.111, 14.418, true, 'cisarsky-ostrov');
INSERT INTO analytic.camea_bikecounters_in_contract (id, location_id, location_name, route_name, lat, lng, in_camea_contract, old_id) VALUES(40, 'BC_KO-LEPR', 'Letňany', 'A 27', 50.127, 14.507, true, 'letnany');

-- analytic.v_camea_bikecounters_24h_missing source

CREATE OR REPLACE VIEW analytic.v_camea_bikecounters_24h_missing
AS SELECT max(to_timestamp((bd.measured_from / 1000)::double precision)) AS last_refresh,
    bl.name
   FROM bicyclecounters_detections bd
     JOIN bicyclecounters_locations bl ON bd.locations_id::text = bl.id::text
  GROUP BY bl.name
 HAVING max(to_timestamp((bd.measured_from / 1000)::double precision)) < (now() - '1 day'::interval) AND max(to_timestamp((bd.measured_from / 1000)::double precision)) >= (CURRENT_DATE - '3 mons'::interval);


-- analytic.v_camea_bikecounters_all_data_history source

CREATE OR REPLACE VIEW analytic.v_camea_bikecounters_all_data_history
AS SELECT to_timestamp((det.measured_from / 1000)::double precision)::date AS datum,
    det.locations_id,
    loc.name AS location_name,
    old_1.in_camea_contract,
    replace(loc.route::text, ' '::text, ''::text) AS route,
    dir.name AS direction_name,
        CASE
            WHEN sum(det.value) IS NULL THEN 0::bigint
            ELSE sum(det.value)
        END AS detections
   FROM bicyclecounters_detections det
     LEFT JOIN bicyclecounters_locations loc ON det.locations_id::text = loc.id::text
     LEFT JOIN bicyclecounters_directions dir ON det.directions_id::text = dir.id::text
     LEFT JOIN analytic.camea_bikecounters_in_contract old_1 ON loc.vendor_id::text = old_1.location_id::text
  GROUP BY (to_timestamp((det.measured_from / 1000)::double precision)::date), det.locations_id, loc.name, old_1.in_camea_contract, (replace(loc.route::text, ' '::text, ''::text)), dir.name;


-- analytic.v_camea_bikecounters_all_data_table_today source

CREATE OR REPLACE VIEW analytic.v_camea_bikecounters_all_data_table_today
AS SELECT t1.datum,
    t1.locations_id,
    t1.location_name,
    t1.route,
    t1.direction_name,
    t1.in_camea_contract,
    t1.dir1det,
        CASE
            WHEN t2.direction_name::text = t1.direction_name::text THEN 'není'::character varying
            ELSE t2.direction_name
        END AS direction2_name,
    t2.dir2det,
    t3.temperature
   FROM ( SELECT to_timestamp((det.measured_from / 1000)::double precision) AS datum,
            det.locations_id,
            loc.name AS location_name,
            replace(loc.route::text, ' '::text, ''::text) AS route,
            dir.name AS direction_name,
            old_1.in_camea_contract,
                CASE
                    WHEN sum(det.value) IS NULL THEN 0::bigint
                    ELSE sum(det.value)
                END AS dir1det
           FROM bicyclecounters_detections det
             LEFT JOIN bicyclecounters_locations loc ON det.locations_id::text = loc.id::text
             LEFT JOIN bicyclecounters_directions dir ON det.directions_id::text = dir.id::text
             LEFT JOIN analytic.camea_bikecounters_in_contract old_1 ON loc.vendor_id::text = old_1.location_id::text
          WHERE (det.directions_id::text IN ( SELECT min(bicyclecounters_directions.id::text) AS min
                   FROM bicyclecounters_directions
                  GROUP BY bicyclecounters_directions.locations_id)) AND (to_timestamp((det.measured_from / 1000)::double precision)::date IN ( SELECT max(to_timestamp((bicyclecounters_detections.measured_from / 1000)::double precision)::date) AS max
                   FROM bicyclecounters_detections))
          GROUP BY (to_timestamp((det.measured_from / 1000)::double precision)), det.locations_id, loc.name, (replace(loc.route::text, ' '::text, ''::text)), dir.name, old_1.in_camea_contract) t1
     LEFT JOIN ( SELECT to_timestamp((det2.measured_from / 1000)::double precision) AS datum,
            det2.locations_id,
            loc.name AS location_name,
            dir.vendor_id AS direction_id,
            dir.name AS direction_name,
                CASE
                    WHEN sum(det2.value) IS NULL THEN 0::bigint
                    ELSE sum(det2.value)
                END AS dir2det
           FROM bicyclecounters_detections det2
             LEFT JOIN bicyclecounters_locations loc ON det2.locations_id::text = loc.id::text
             LEFT JOIN bicyclecounters_directions dir ON det2.directions_id::text = dir.id::text
          WHERE (det2.directions_id::text IN ( SELECT max(bicyclecounters_directions.id::text) AS max
                   FROM bicyclecounters_directions
                  GROUP BY bicyclecounters_directions.locations_id)) AND (to_timestamp((det2.measured_from / 1000)::double precision)::date IN ( SELECT max(to_timestamp((bicyclecounters_detections.measured_from / 1000)::double precision)::date) AS max
                   FROM bicyclecounters_detections))
          GROUP BY (to_timestamp((det2.measured_from / 1000)::double precision)), det2.locations_id, loc.name, dir.vendor_id, dir.name) t2 ON t1.datum = t2.datum AND t1.locations_id::text = t2.locations_id::text
     LEFT JOIN ( SELECT to_timestamp((bicyclecounters_temperatures.measured_from / 1000)::double precision) AS datum,
            bicyclecounters_temperatures.locations_id,
            avg(bicyclecounters_temperatures.value) AS temperature
           FROM bicyclecounters_temperatures
          WHERE (to_timestamp((bicyclecounters_temperatures.measured_from / 1000)::double precision)::date IN ( SELECT max(to_timestamp((bicyclecounters_detections.measured_from / 1000)::double precision)::date) AS max
                   FROM bicyclecounters_detections))
          GROUP BY (to_timestamp((bicyclecounters_temperatures.measured_from / 1000)::double precision)), bicyclecounters_temperatures.locations_id) t3 ON t1.datum = t3.datum AND t1.locations_id::text = t3.locations_id::text;


-- analytic.v_camea_bikecounters_all_data_today source

CREATE OR REPLACE VIEW analytic.v_camea_bikecounters_all_data_today
AS SELECT t1.datum,
    t1.locations_id,
    t1.location_name,
    t1.in_camea_contract,
    t1.lat,
    t1.lng,
    t1.route,
    t1.directions_id,
    t1.direction_name,
    t1.direction_num,
    t1.detections,
    t2.temperature
   FROM ( SELECT to_timestamp((det.measured_from / 1000)::double precision) AS datum,
            det.locations_id,
            loc.name AS location_name,
            old_1.in_camea_contract,
            loc.lat,
            loc.lng,
            replace(loc.route::text, ' '::text, ''::text) AS route,
            det.directions_id,
            dir.name AS direction_name,
                CASE
                    WHEN (det.directions_id::text IN ( SELECT min(bicyclecounters_detections.directions_id::text) AS min
                       FROM bicyclecounters_detections
                      GROUP BY bicyclecounters_detections.locations_id)) THEN 'Směr 1'::text
                    ELSE 'Směr 2'::text
                END AS direction_num,
                CASE
                    WHEN sum(det.value) IS NULL THEN 0::bigint
                    ELSE sum(det.value)
                END AS detections
           FROM bicyclecounters_detections det
             LEFT JOIN bicyclecounters_locations loc ON det.locations_id::text = loc.id::text
             LEFT JOIN bicyclecounters_directions dir ON det.directions_id::text = dir.id::text
             LEFT JOIN analytic.camea_bikecounters_in_contract old_1 ON loc.vendor_id::text = old_1.location_id::text
          WHERE (to_timestamp((det.measured_from / 1000)::double precision)::date IN ( SELECT max(to_timestamp((bicyclecounters_detections.measured_from / 1000)::double precision)::date) AS max
                   FROM bicyclecounters_detections))
          GROUP BY (to_timestamp((det.measured_from / 1000)::double precision)), det.locations_id, loc.name, old_1.in_camea_contract, loc.lat, loc.lng, (replace(loc.route::text, ' '::text, ''::text)), det.directions_id, dir.name, (
                CASE
                    WHEN (det.directions_id::text IN ( SELECT min(bicyclecounters_detections.directions_id::text) AS min
                       FROM bicyclecounters_detections
                      GROUP BY bicyclecounters_detections.locations_id)) THEN 'Směr 1'::text
                    ELSE 'Směr 2'::text
                END)) t1
     LEFT JOIN ( SELECT to_timestamp((bicyclecounters_temperatures.measured_from / 1000)::double precision) AS datum,
            bicyclecounters_temperatures.locations_id,
            avg(bicyclecounters_temperatures.value) AS temperature
           FROM bicyclecounters_temperatures
          WHERE (to_timestamp((bicyclecounters_temperatures.measured_from / 1000)::double precision)::date IN ( SELECT max(to_timestamp((bicyclecounters_temperatures_1.measured_from / 1000)::double precision)::date) AS max
                   FROM bicyclecounters_temperatures bicyclecounters_temperatures_1))
          GROUP BY (to_timestamp((bicyclecounters_temperatures.measured_from / 1000)::double precision)), bicyclecounters_temperatures.locations_id) t2 ON t1.datum = t2.datum AND t1.locations_id::text = t2.locations_id::text;


-- analytic.v_camea_bikecounters_data_disbalance source

CREATE OR REPLACE VIEW analytic.v_camea_bikecounters_data_disbalance
AS SELECT d1.datum,
    d1.locations_id,
    d1.location_name,
    d1.direction_id AS dir1id,
    d1.direction_name AS dir1_name,
        CASE
            WHEN d1.dir1det IS NULL THEN 0::bigint
            ELSE d1.dir1det
        END AS dir1det,
    d1.in_camea_contract,
        CASE
            WHEN d2.direction_id::text = d1.direction_id::text THEN 'není'::character varying
            ELSE d2.direction_id
        END AS dir2id,
        CASE
            WHEN d2.direction_id::text = d1.direction_id::text THEN 'není'::character varying
            ELSE d2.direction_name
        END AS dir2_name,
        CASE
            WHEN d2.direction_id::text = d1.direction_id::text THEN 0::bigint
            WHEN d2.dir2det IS NULL THEN 0::bigint
            ELSE d2.dir2det
        END AS dir2det
   FROM ( SELECT to_timestamp((det.measured_from / 1000)::double precision)::date AS datum,
            det.locations_id,
            loc.name AS location_name,
            dir.vendor_id AS direction_id,
            dir.name AS direction_name,
            old_1.in_camea_contract,
            sum(det.value) AS dir1det
           FROM bicyclecounters_detections det
             LEFT JOIN bicyclecounters_locations loc ON det.locations_id::text = loc.id::text
             LEFT JOIN bicyclecounters_directions dir ON det.directions_id::text = dir.id::text
             LEFT JOIN analytic.camea_bikecounters_in_contract old_1 ON loc.vendor_id::text = old_1.location_id::text
          WHERE (det.directions_id::text IN ( SELECT max(bicyclecounters_directions.id::text) AS max
                   FROM bicyclecounters_directions
                  GROUP BY bicyclecounters_directions.locations_id))
          GROUP BY (to_timestamp((det.measured_from / 1000)::double precision)::date), det.locations_id, loc.name, dir.vendor_id, dir.name, old_1.in_camea_contract) d1
     LEFT JOIN ( SELECT to_timestamp((det2.measured_from / 1000)::double precision)::date AS datum,
            det2.locations_id,
            loc.name AS location_name,
            dir.vendor_id AS direction_id,
            dir.name AS direction_name,
            sum(det2.value) AS dir2det
           FROM bicyclecounters_detections det2
             LEFT JOIN bicyclecounters_locations loc ON det2.locations_id::text = loc.id::text
             LEFT JOIN bicyclecounters_directions dir ON det2.directions_id::text = dir.id::text
          WHERE (det2.directions_id::text IN ( SELECT min(bicyclecounters_directions.id::text) AS min
                   FROM bicyclecounters_directions
                  GROUP BY bicyclecounters_directions.locations_id))
          GROUP BY (to_timestamp((det2.measured_from / 1000)::double precision)::date), det2.locations_id, loc.name, dir.vendor_id, dir.name) d2 ON d1.datum = d2.datum AND d1.locations_id::text = d2.locations_id::text;


-- analytic.v_camea_bikecounters_data_quality source

CREATE OR REPLACE VIEW analytic.v_camea_bikecounters_data_quality
AS SELECT to_timestamp((det.measured_from / 1000)::double precision)::date AS date,
    loc.vendor_id AS location_id,
    loc.name AS location_name,
    dir.vendor_id AS direction_id,
    dir.name AS direction_name,
    count(det.locations_id) AS lines,
    count(det.value) AS detections,
    count(det.value)::numeric / count(det.locations_id)::numeric AS ratio,
    old_1.in_camea_contract
   FROM bicyclecounters_detections det
     LEFT JOIN bicyclecounters_locations loc ON det.locations_id::text = loc.id::text
     LEFT JOIN bicyclecounters_directions dir ON det.directions_id::text = dir.id::text
     LEFT JOIN analytic.camea_bikecounters_in_contract old_1 ON loc.vendor_id::text = old_1.location_id::text
  GROUP BY (to_timestamp((det.measured_from / 1000)::double precision)::date), loc.vendor_id, loc.name, dir.vendor_id, dir.name, old_1.in_camea_contract;


-- analytic.v_camea_bikecounters_last_update source

CREATE OR REPLACE VIEW analytic.v_camea_bikecounters_last_update
AS SELECT max(timezone('Europe/Prague'::text, to_timestamp((bicyclecounters_detections.measured_to / 1000)::double precision))) AS max
   FROM bicyclecounters_detections;


-- analytic.v_camea_bikecounters_name_table source

CREATE OR REPLACE VIEW analytic.v_camea_bikecounters_name_table
AS SELECT loc.id AS location_id,
    loc.vendor_id AS loc_vendor_id,
    loc.name AS location_name,
    loc.route,
    loc.lat,
    loc.lng,
    dir.id AS direction_id,
    dir.vendor_id AS dir_vendor_id,
    dir.name AS direction_name,
    old_1.in_camea_contract
   FROM bicyclecounters_locations loc
     JOIN bicyclecounters_directions dir ON loc.id::text = dir.locations_id::text
     JOIN analytic.camea_bikecounters_in_contract old_1 ON loc.vendor_id::text = old_1.location_id::text;


-- analytic.v_camea_data_disbalance source

CREATE OR REPLACE VIEW analytic.v_camea_data_disbalance
AS SELECT d1.datum,
    d1.locations_id,
    d1.location_name,
    d1.direction_id AS dir1id,
    d1.direction_name AS dir1_name,
        CASE
            WHEN d1.dir1det IS NULL THEN 0::bigint
            ELSE d1.dir1det
        END AS dir1det,
    d1.in_camea_contract,
        CASE
            WHEN d2.direction_id::text = d1.direction_id::text THEN 'není'::character varying
            ELSE d2.direction_id
        END AS dir2id,
        CASE
            WHEN d2.direction_id::text = d1.direction_id::text THEN 'není'::character varying
            ELSE d2.direction_name
        END AS dir2_name,
        CASE
            WHEN d2.direction_id::text = d1.direction_id::text THEN 0::bigint
            WHEN d2.dir2det IS NULL THEN 0::bigint
            ELSE d2.dir2det
        END AS dir2det
   FROM ( SELECT to_timestamp((det.measured_from / 1000)::double precision)::date AS datum,
            det.locations_id,
            loc.name AS location_name,
            dir.vendor_id AS direction_id,
            dir.name AS direction_name,
            old_1.in_camea_contract,
            sum(det.value) AS dir1det
           FROM bicyclecounters_detections det
             LEFT JOIN bicyclecounters_locations loc ON det.locations_id::text = loc.id::text
             LEFT JOIN bicyclecounters_directions dir ON det.directions_id::text = dir.id::text
             LEFT JOIN analytic.camea_bikecounters_in_contract old_1 ON loc.vendor_id::text = old_1.location_id::text
          WHERE (det.directions_id::text IN ( SELECT max(bicyclecounters_directions.id::text) AS max
                   FROM bicyclecounters_directions
                  GROUP BY bicyclecounters_directions.locations_id))
          GROUP BY (to_timestamp((det.measured_from / 1000)::double precision)::date), det.locations_id, loc.name, dir.vendor_id, dir.name, old_1.in_camea_contract) d1
     LEFT JOIN ( SELECT to_timestamp((det2.measured_from / 1000)::double precision)::date AS datum,
            det2.locations_id,
            loc.name AS location_name,
            dir.vendor_id AS direction_id,
            dir.name AS direction_name,
            sum(det2.value) AS dir2det
           FROM bicyclecounters_detections det2
             LEFT JOIN bicyclecounters_locations loc ON det2.locations_id::text = loc.id::text
             LEFT JOIN bicyclecounters_directions dir ON det2.directions_id::text = dir.id::text
          WHERE (det2.directions_id::text IN ( SELECT min(bicyclecounters_directions.id::text) AS min
                   FROM bicyclecounters_directions
                  GROUP BY bicyclecounters_directions.locations_id))
          GROUP BY (to_timestamp((det2.measured_from / 1000)::double precision)::date), det2.locations_id, loc.name, dir.vendor_id, dir.name) d2 ON d1.datum = d2.datum AND d1.locations_id::text = d2.locations_id::text;


-- analytic.v_camea_data_quality source

CREATE OR REPLACE VIEW analytic.v_camea_data_quality
AS SELECT to_timestamp((det.measured_from / 1000)::double precision)::date AS date,
    loc.vendor_id AS location_id,
    loc.name AS location_name,
    dir.vendor_id AS direction_id,
    dir.name AS direction_name,
    count(det.locations_id) AS lines,
    count(det.value) AS detections,
    count(det.value)::numeric / count(det.locations_id)::numeric AS ratio,
    old_1.in_camea_contract
   FROM bicyclecounters_detections det
     LEFT JOIN bicyclecounters_locations loc ON det.locations_id::text = loc.id::text
     LEFT JOIN bicyclecounters_directions dir ON det.directions_id::text = dir.id::text
     LEFT JOIN analytic.camea_bikecounters_in_contract old_1 ON loc.vendor_id::text = old_1.location_id::text
  GROUP BY (to_timestamp((det.measured_from / 1000)::double precision)::date), loc.vendor_id, loc.name, dir.vendor_id, dir.name, old_1.in_camea_contract;


-- analytic.v_camea_last_update source

CREATE OR REPLACE VIEW analytic.v_camea_last_update
AS SELECT max(timezone('Europe/Prague'::text, to_timestamp((bicyclecounters_detections.measured_to / 1000)::double precision))) AS max
   FROM bicyclecounters_detections;
