/* Replace with your SQL commands */


-- drop existing analytic views connected to tables we change
DROP VIEW IF EXISTS  analytic.v_camea_bikecounters_24h_missing;
DROP VIEW IF EXISTS analytic.v_camea_bikecounters_24h_missing;
DROP VIEW IF EXISTS analytic.v_camea_bikecounters_all_data_history;
DROP VIEW IF EXISTS analytic.v_camea_bikecounters_all_data_table_today;
DROP VIEW IF EXISTS analytic.v_camea_bikecounters_all_data_today;
DROP VIEW IF EXISTS analytic.v_camea_bikecounters_data_disbalance;
DROP VIEW IF EXISTS analytic.v_camea_bikecounters_data_quality;
DROP VIEW IF EXISTS analytic.v_camea_bikecounters_last_update;
DROP VIEW IF EXISTS analytic.v_camea_bikecounters_name_table;
DROP VIEW IF EXISTS analytic.v_camea_data_disbalance;
DROP VIEW IF EXISTS analytic.v_camea_data_quality;
DROP VIEW IF EXISTS analytic.v_camea_last_update;

DROP VIEW IF EXISTS analytic.v_camea_bikecounters_history;
DROP VIEW IF EXISTS analytic.v_camea_bikecounters_history_table;
DROP VIEW IF EXISTS analytic.v_bicyclecounters_24h_missing;

-- drop only view in bicycle_counters schema
DROP VIEW IF EXISTS v_camea_bikecounters_data_month;

-- drop table detections
DROP TABLE IF EXISTS bicyclecounters_detections;
DROP TABLE IF EXISTS bicyclecounters_temperatures;

--rename original table
ALTER TABLE bicyclecounters_detections_old RENAME TO bicyclecounters_detections;
ALTER TABLE bicyclecounters_temperatures_old RENAME TO bicyclecounters_temperatures;


--rename partitions 
ALTER TABLE bicyclecounters_detections_min_old RENAME TO bicyclecounters_detections_min;
ALTER TABLE bicyclecounters_detections_y2019_old RENAME TO bicyclecounters_detections_y2019;
ALTER TABLE bicyclecounters_detections_y2020_old RENAME TO bicyclecounters_detections_y2020;
ALTER TABLE bicyclecounters_detections_y2021_old RENAME TO bicyclecounters_detections_y2021;
ALTER TABLE bicyclecounters_detections_y2022_old RENAME TO bicyclecounters_detections_y2022;
ALTER TABLE bicyclecounters_detections_y2023_old RENAME TO bicyclecounters_detections_y2023;
ALTER TABLE bicyclecounters_detections_y2024_old RENAME TO bicyclecounters_detections_y2024;
ALTER TABLE bicyclecounters_detections_y2025_old RENAME TO bicyclecounters_detections_y2025;
ALTER TABLE bicyclecounters_detections_y2026_old RENAME TO bicyclecounters_detections_y2026;
ALTER TABLE bicyclecounters_detections_y2027up_old RENAME TO bicyclecounters_detections_y2027up;

--rename back partitions 
ALTER TABLE bicyclecounters_temperatures_min_old RENAME TO bicyclecounters_temperatures_min;
ALTER TABLE bicyclecounters_temperatures_y2019_old RENAME TO bicyclecounters_temperatures_y2019;
ALTER TABLE bicyclecounters_temperatures_y2020_old RENAME TO bicyclecounters_temperatures_y2020;
ALTER TABLE bicyclecounters_temperatures_y2021_old RENAME TO bicyclecounters_temperatures_y2021;
ALTER TABLE bicyclecounters_temperatures_y2022_old RENAME TO bicyclecounters_temperatures_y2022;
ALTER TABLE bicyclecounters_temperatures_y2023_old RENAME TO bicyclecounters_temperatures_y2023;
ALTER TABLE bicyclecounters_temperatures_y2024_old RENAME TO bicyclecounters_temperatures_y2024;
ALTER TABLE bicyclecounters_temperatures_y2025_old RENAME TO bicyclecounters_temperatures_y2025;
ALTER TABLE bicyclecounters_temperatures_y2026_old RENAME TO bicyclecounters_temperatures_y2026;
ALTER TABLE bicyclecounters_temperatures_y2027up_old RENAME TO bicyclecounters_temperatures_y2027up;


-- analytic.v_camea_bikecounters_24h_missing source

CREATE OR REPLACE VIEW analytic.v_camea_bikecounters_24h_missing
AS SELECT max(to_timestamp((bd.measured_from / 1000)::double precision)) AS last_refresh,
    bl.name
   FROM bicyclecounters_detections bd
     JOIN bicyclecounters_locations bl ON bd.locations_id::text = bl.id::text
  GROUP BY bl.name
 HAVING max(to_timestamp((bd.measured_from / 1000)::double precision)) < (now() - '1 day'::interval) AND max(to_timestamp((bd.measured_from / 1000)::double precision)) >= (CURRENT_DATE - '3 mons'::interval);

-- analytic.v_camea_bikecounters_all_data_history source

CREATE OR REPLACE VIEW analytic.v_camea_bikecounters_all_data_history
AS SELECT to_timestamp((det.measured_from / 1000)::double precision)::date AS datum,
    det.locations_id,
    loc.name AS location_name,
    old_1.in_camea_contract,
    replace(loc.route::text, ' '::text, ''::text) AS route,
    dir.name AS direction_name,
        CASE
            WHEN sum(det.value) IS NULL THEN 0::bigint
            ELSE sum(det.value)
        END AS detections
   FROM bicyclecounters_detections det
     LEFT JOIN bicyclecounters_locations loc ON det.locations_id::text = loc.id::text
     LEFT JOIN bicyclecounters_directions dir ON det.directions_id::text = dir.id::text
     LEFT JOIN analytic.camea_bikecounters_in_contract old_1 ON loc.vendor_id::text = old_1.location_id::text
  GROUP BY (to_timestamp((det.measured_from / 1000)::double precision)::date), det.locations_id, loc.name, old_1.in_camea_contract, (replace(loc.route::text, ' '::text, ''::text)), dir.name;


-- analytic.v_camea_bikecounters_all_data_table_today source

CREATE OR REPLACE VIEW analytic.v_camea_bikecounters_all_data_table_today
AS SELECT t1.datum,
    t1.locations_id,
    t1.location_name,
    t1.route,
    t1.direction_name,
    t1.in_camea_contract,
    t1.dir1det,
        CASE
            WHEN t2.direction_name::text = t1.direction_name::text THEN 'není'::character varying
            ELSE t2.direction_name
        END AS direction2_name,
    t2.dir2det,
    t3.temperature
   FROM ( SELECT to_timestamp((det.measured_from / 1000)::double precision) AS datum,
            det.locations_id,
            loc.name AS location_name,
            replace(loc.route::text, ' '::text, ''::text) AS route,
            dir.name AS direction_name,
            old_1.in_camea_contract,
                CASE
                    WHEN sum(det.value) IS NULL THEN 0::bigint
                    ELSE sum(det.value)
                END AS dir1det
           FROM bicyclecounters_detections det
             LEFT JOIN bicyclecounters_locations loc ON det.locations_id::text = loc.id::text
             LEFT JOIN bicyclecounters_directions dir ON det.directions_id::text = dir.id::text
             LEFT JOIN analytic.camea_bikecounters_in_contract old_1 ON loc.vendor_id::text = old_1.location_id::text
          WHERE (det.directions_id::text IN ( SELECT min(bicyclecounters_directions.id::text) AS min
                   FROM bicyclecounters_directions
                  GROUP BY bicyclecounters_directions.locations_id)) AND (to_timestamp((det.measured_from / 1000)::double precision)::date IN ( SELECT max(to_timestamp((bicyclecounters_detections.measured_from / 1000)::double precision)::date) AS max
                   FROM bicyclecounters_detections))
          GROUP BY (to_timestamp((det.measured_from / 1000)::double precision)), det.locations_id, loc.name, (replace(loc.route::text, ' '::text, ''::text)), dir.name, old_1.in_camea_contract) t1
     LEFT JOIN ( SELECT to_timestamp((det2.measured_from / 1000)::double precision) AS datum,
            det2.locations_id,
            loc.name AS location_name,
            dir.vendor_id AS direction_id,
            dir.name AS direction_name,
                CASE
                    WHEN sum(det2.value) IS NULL THEN 0::bigint
                    ELSE sum(det2.value)
                END AS dir2det
           FROM bicyclecounters_detections det2
             LEFT JOIN bicyclecounters_locations loc ON det2.locations_id::text = loc.id::text
             LEFT JOIN bicyclecounters_directions dir ON det2.directions_id::text = dir.id::text
          WHERE (det2.directions_id::text IN ( SELECT max(bicyclecounters_directions.id::text) AS max
                   FROM bicyclecounters_directions
                  GROUP BY bicyclecounters_directions.locations_id)) AND (to_timestamp((det2.measured_from / 1000)::double precision)::date IN ( SELECT max(to_timestamp((bicyclecounters_detections.measured_from / 1000)::double precision)::date) AS max
                   FROM bicyclecounters_detections))
          GROUP BY (to_timestamp((det2.measured_from / 1000)::double precision)), det2.locations_id, loc.name, dir.vendor_id, dir.name) t2 ON t1.datum = t2.datum AND t1.locations_id::text = t2.locations_id::text
     LEFT JOIN ( SELECT to_timestamp((bicyclecounters_temperatures.measured_from / 1000)::double precision) AS datum,
            bicyclecounters_temperatures.locations_id,
            avg(bicyclecounters_temperatures.value) AS temperature
           FROM bicyclecounters_temperatures
          WHERE (to_timestamp((bicyclecounters_temperatures.measured_from / 1000)::double precision)::date IN ( SELECT max(to_timestamp((bicyclecounters_detections.measured_from / 1000)::double precision)::date) AS max
                   FROM bicyclecounters_detections))
          GROUP BY (to_timestamp((bicyclecounters_temperatures.measured_from / 1000)::double precision)), bicyclecounters_temperatures.locations_id) t3 ON t1.datum = t3.datum AND t1.locations_id::text = t3.locations_id::text;

-- analytic.v_camea_bikecounters_all_data_today source

CREATE OR REPLACE VIEW analytic.v_camea_bikecounters_all_data_today
AS SELECT t1.datum,
    t1.locations_id,
    t1.location_name,
    t1.in_camea_contract,
    t1.lat,
    t1.lng,
    t1.route,
    t1.directions_id,
    t1.direction_name,
    t1.direction_num,
    t1.detections,
    t2.temperature
   FROM ( SELECT to_timestamp((det.measured_from / 1000)::double precision) AS datum,
            det.locations_id,
            loc.name AS location_name,
            old_1.in_camea_contract,
            loc.lat,
            loc.lng,
            replace(loc.route::text, ' '::text, ''::text) AS route,
            det.directions_id,
            dir.name AS direction_name,
                CASE
                    WHEN (det.directions_id::text IN ( SELECT min(bicyclecounters_detections.directions_id::text) AS min
                       FROM bicyclecounters_detections
                      GROUP BY bicyclecounters_detections.locations_id)) THEN 'Směr 1'::text
                    ELSE 'Směr 2'::text
                END AS direction_num,
                CASE
                    WHEN sum(det.value) IS NULL THEN 0::bigint
                    ELSE sum(det.value)
                END AS detections
           FROM bicyclecounters_detections det
             LEFT JOIN bicyclecounters_locations loc ON det.locations_id::text = loc.id::text
             LEFT JOIN bicyclecounters_directions dir ON det.directions_id::text = dir.id::text
             LEFT JOIN analytic.camea_bikecounters_in_contract old_1 ON loc.vendor_id::text = old_1.location_id::text
          WHERE (to_timestamp((det.measured_from / 1000)::double precision)::date IN ( SELECT max(to_timestamp((bicyclecounters_detections.measured_from / 1000)::double precision)::date) AS max
                   FROM bicyclecounters_detections))
          GROUP BY (to_timestamp((det.measured_from / 1000)::double precision)), det.locations_id, loc.name, old_1.in_camea_contract, loc.lat, loc.lng, (replace(loc.route::text, ' '::text, ''::text)), det.directions_id, dir.name, (
                CASE
                    WHEN (det.directions_id::text IN ( SELECT min(bicyclecounters_detections.directions_id::text) AS min
                       FROM bicyclecounters_detections
                      GROUP BY bicyclecounters_detections.locations_id)) THEN 'Směr 1'::text
                    ELSE 'Směr 2'::text
                END)) t1
     LEFT JOIN ( SELECT to_timestamp((bicyclecounters_temperatures.measured_from / 1000)::double precision) AS datum,
            bicyclecounters_temperatures.locations_id,
            avg(bicyclecounters_temperatures.value) AS temperature
           FROM bicyclecounters_temperatures
          WHERE (to_timestamp((bicyclecounters_temperatures.measured_from / 1000)::double precision)::date IN ( SELECT max(to_timestamp((bicyclecounters_temperatures_1.measured_from / 1000)::double precision)::date) AS max
                   FROM bicyclecounters_temperatures bicyclecounters_temperatures_1))
          GROUP BY (to_timestamp((bicyclecounters_temperatures.measured_from / 1000)::double precision)), bicyclecounters_temperatures.locations_id) t2 ON t1.datum = t2.datum AND t1.locations_id::text = t2.locations_id::text;


-- analytic.v_camea_bikecounters_data_disbalance source

CREATE OR REPLACE VIEW analytic.v_camea_bikecounters_data_disbalance
AS SELECT d1.datum,
    d1.locations_id,
    d1.location_name,
    d1.direction_id AS dir1id,
    d1.direction_name AS dir1_name,
        CASE
            WHEN d1.dir1det IS NULL THEN 0::bigint
            ELSE d1.dir1det
        END AS dir1det,
    d1.in_camea_contract,
        CASE
            WHEN d2.direction_id::text = d1.direction_id::text THEN 'není'::character varying
            ELSE d2.direction_id
        END AS dir2id,
        CASE
            WHEN d2.direction_id::text = d1.direction_id::text THEN 'není'::character varying
            ELSE d2.direction_name
        END AS dir2_name,
        CASE
            WHEN d2.direction_id::text = d1.direction_id::text THEN 0::bigint
            WHEN d2.dir2det IS NULL THEN 0::bigint
            ELSE d2.dir2det
        END AS dir2det
   FROM ( SELECT to_timestamp((det.measured_from / 1000)::double precision)::date AS datum,
            det.locations_id,
            loc.name AS location_name,
            dir.vendor_id AS direction_id,
            dir.name AS direction_name,
            old_1.in_camea_contract,
            sum(det.value) AS dir1det
           FROM bicyclecounters_detections det
             LEFT JOIN bicyclecounters_locations loc ON det.locations_id::text = loc.id::text
             LEFT JOIN bicyclecounters_directions dir ON det.directions_id::text = dir.id::text
             LEFT JOIN analytic.camea_bikecounters_in_contract old_1 ON loc.vendor_id::text = old_1.location_id::text
          WHERE (det.directions_id::text IN ( SELECT max(bicyclecounters_directions.id::text) AS max
                   FROM bicyclecounters_directions
                  GROUP BY bicyclecounters_directions.locations_id))
          GROUP BY (to_timestamp((det.measured_from / 1000)::double precision)::date), det.locations_id, loc.name, dir.vendor_id, dir.name, old_1.in_camea_contract) d1
     LEFT JOIN ( SELECT to_timestamp((det2.measured_from / 1000)::double precision)::date AS datum,
            det2.locations_id,
            loc.name AS location_name,
            dir.vendor_id AS direction_id,
            dir.name AS direction_name,
            sum(det2.value) AS dir2det
           FROM bicyclecounters_detections det2
             LEFT JOIN bicyclecounters_locations loc ON det2.locations_id::text = loc.id::text
             LEFT JOIN bicyclecounters_directions dir ON det2.directions_id::text = dir.id::text
          WHERE (det2.directions_id::text IN ( SELECT min(bicyclecounters_directions.id::text) AS min
                   FROM bicyclecounters_directions
                  GROUP BY bicyclecounters_directions.locations_id))
          GROUP BY (to_timestamp((det2.measured_from / 1000)::double precision)::date), det2.locations_id, loc.name, dir.vendor_id, dir.name) d2 ON d1.datum = d2.datum AND d1.locations_id::text = d2.locations_id::text;

-- analytic.v_camea_bikecounters_data_quality source

CREATE OR REPLACE VIEW analytic.v_camea_bikecounters_data_quality
AS SELECT to_timestamp((det.measured_from / 1000)::double precision)::date AS date,
    loc.vendor_id AS location_id,
    loc.name AS location_name,
    dir.vendor_id AS direction_id,
    dir.name AS direction_name,
    count(det.locations_id) AS lines,
    count(det.value) AS detections,
    count(det.value)::numeric / count(det.locations_id)::numeric AS ratio,
    old_1.in_camea_contract
   FROM bicyclecounters_detections det
     LEFT JOIN bicyclecounters_locations loc ON det.locations_id::text = loc.id::text
     LEFT JOIN bicyclecounters_directions dir ON det.directions_id::text = dir.id::text
     LEFT JOIN analytic.camea_bikecounters_in_contract old_1 ON loc.vendor_id::text = old_1.location_id::text
  GROUP BY (to_timestamp((det.measured_from / 1000)::double precision)::date), loc.vendor_id, loc.name, dir.vendor_id, dir.name, old_1.in_camea_contract;


-- analytic.v_camea_bikecounters_last_update source

CREATE OR REPLACE VIEW analytic.v_camea_bikecounters_last_update
AS SELECT max(timezone('Europe/Prague'::text, to_timestamp((bicyclecounters_detections.measured_to / 1000)::double precision))) AS max
   FROM bicyclecounters_detections;

-- analytic.v_camea_bikecounters_name_table source

CREATE OR REPLACE VIEW analytic.v_camea_bikecounters_name_table
AS SELECT loc.id AS location_id,
    loc.vendor_id AS loc_vendor_id,
    loc.name AS location_name,
    loc.route,
    loc.lat,
    loc.lng,
    dir.id AS direction_id,
    dir.vendor_id AS dir_vendor_id,
    dir.name AS direction_name,
    old_1.in_camea_contract
   FROM bicyclecounters_locations loc
     JOIN bicyclecounters_directions dir ON loc.id::text = dir.locations_id::text
     JOIN analytic.camea_bikecounters_in_contract old_1 ON loc.vendor_id::text = old_1.location_id::text;


-- analytic.v_camea_data_disbalance source

CREATE OR REPLACE VIEW analytic.v_camea_data_disbalance
AS SELECT d1.datum,
    d1.locations_id,
    d1.location_name,
    d1.direction_id AS dir1id,
    d1.direction_name AS dir1_name,
        CASE
            WHEN d1.dir1det IS NULL THEN 0::bigint
            ELSE d1.dir1det
        END AS dir1det,
    d1.in_camea_contract,
        CASE
            WHEN d2.direction_id::text = d1.direction_id::text THEN 'není'::character varying
            ELSE d2.direction_id
        END AS dir2id,
        CASE
            WHEN d2.direction_id::text = d1.direction_id::text THEN 'není'::character varying
            ELSE d2.direction_name
        END AS dir2_name,
        CASE
            WHEN d2.direction_id::text = d1.direction_id::text THEN 0::bigint
            WHEN d2.dir2det IS NULL THEN 0::bigint
            ELSE d2.dir2det
        END AS dir2det
   FROM ( SELECT to_timestamp((det.measured_from / 1000)::double precision)::date AS datum,
            det.locations_id,
            loc.name AS location_name,
            dir.vendor_id AS direction_id,
            dir.name AS direction_name,
            old_1.in_camea_contract,
            sum(det.value) AS dir1det
           FROM bicyclecounters_detections det
             LEFT JOIN bicyclecounters_locations loc ON det.locations_id::text = loc.id::text
             LEFT JOIN bicyclecounters_directions dir ON det.directions_id::text = dir.id::text
             LEFT JOIN analytic.camea_bikecounters_in_contract old_1 ON loc.vendor_id::text = old_1.location_id::text
          WHERE (det.directions_id::text IN ( SELECT max(bicyclecounters_directions.id::text) AS max
                   FROM bicyclecounters_directions
                  GROUP BY bicyclecounters_directions.locations_id))
          GROUP BY (to_timestamp((det.measured_from / 1000)::double precision)::date), det.locations_id, loc.name, dir.vendor_id, dir.name, old_1.in_camea_contract) d1
     LEFT JOIN ( SELECT to_timestamp((det2.measured_from / 1000)::double precision)::date AS datum,
            det2.locations_id,
            loc.name AS location_name,
            dir.vendor_id AS direction_id,
            dir.name AS direction_name,
            sum(det2.value) AS dir2det
           FROM bicyclecounters_detections det2
             LEFT JOIN bicyclecounters_locations loc ON det2.locations_id::text = loc.id::text
             LEFT JOIN bicyclecounters_directions dir ON det2.directions_id::text = dir.id::text
          WHERE (det2.directions_id::text IN ( SELECT min(bicyclecounters_directions.id::text) AS min
                   FROM bicyclecounters_directions
                  GROUP BY bicyclecounters_directions.locations_id))
          GROUP BY (to_timestamp((det2.measured_from / 1000)::double precision)::date), det2.locations_id, loc.name, dir.vendor_id, dir.name) d2 ON d1.datum = d2.datum AND d1.locations_id::text = d2.locations_id::text;

-- analytic.v_camea_data_quality source

CREATE OR REPLACE VIEW analytic.v_camea_data_quality
AS SELECT to_timestamp((det.measured_from / 1000)::double precision)::date AS date,
    loc.vendor_id AS location_id,
    loc.name AS location_name,
    dir.vendor_id AS direction_id,
    dir.name AS direction_name,
    count(det.locations_id) AS lines,
    count(det.value) AS detections,
    count(det.value)::numeric / count(det.locations_id)::numeric AS ratio,
    old_1.in_camea_contract
   FROM bicyclecounters_detections det
     LEFT JOIN bicyclecounters_locations loc ON det.locations_id::text = loc.id::text
     LEFT JOIN bicyclecounters_directions dir ON det.directions_id::text = dir.id::text
     LEFT JOIN analytic.camea_bikecounters_in_contract old_1 ON loc.vendor_id::text = old_1.location_id::text
  GROUP BY (to_timestamp((det.measured_from / 1000)::double precision)::date), loc.vendor_id, loc.name, dir.vendor_id, dir.name, old_1.in_camea_contract;

-- analytic.v_camea_last_update source

CREATE OR REPLACE VIEW analytic.v_camea_last_update
AS SELECT max(timezone('Europe/Prague'::text, to_timestamp((bicyclecounters_detections.measured_to / 1000)::double precision))) AS max
   FROM bicyclecounters_detections;

-- analytic.v_bicyclecounters_24h_missing
CREATE OR REPLACE VIEW analytic.v_bicyclecounters_24h_missing
AS SELECT max(to_timestamp((bd.measured_from / 1000)::double precision)) AS last_refresh,
    bl.name
   FROM bicyclecounters_detections bd
     JOIN bicyclecounters_locations bl ON bd.locations_id::text = bl.id::text
  GROUP BY bl.name
 HAVING max(to_timestamp((bd.measured_from / 1000)::double precision)) < (now() - '1 day'::interval) AND max(to_timestamp((bd.measured_from / 1000)::double precision)) >= (CURRENT_DATE - '3 mons'::interval);

COMMENT ON VIEW analytic.v_bicyclecounters_24h_missing IS 'alerting for Martin Lér';

-- bikecounter history

CREATE OR REPLACE VIEW analytic.v_camea_bikecounters_history
AS SELECT to_timestamp((det.measured_from / 1000)::double precision)::date AS datum,
    det.locations_id,
    loc.name AS location_name,
    old_1.in_camea_contract,
    loc.lat AS latitude,
    loc.lng AS longitude,
    replace(loc.route::text, ' '::text, ''::text) AS route,
    det.directions_id,
    dir.name AS direction_name,
        CASE
            WHEN (det.directions_id::text IN ( SELECT min(bicyclecounters_detections.directions_id::text) AS min
               FROM bicycle_counters.bicyclecounters_detections
              GROUP BY bicyclecounters_detections.locations_id)) THEN 'Směr 1'::text
            ELSE 'Směr 2'::text
        END AS direction_num,
        CASE
            WHEN sum(det.value) IS NULL THEN 0::bigint
            ELSE sum(det.value)
        END AS detections
   FROM bicycle_counters.bicyclecounters_detections det
     LEFT JOIN bicycle_counters.bicyclecounters_locations loc ON det.locations_id::text = loc.id::text
     LEFT JOIN bicycle_counters.bicyclecounters_directions dir ON det.directions_id::text = dir.id::text
     LEFT JOIN analytic.camea_bikecounters_in_contract old_1 ON loc.vendor_id::text = old_1.location_id::text
  GROUP BY (to_timestamp((det.measured_from / 1000)::double precision)::date), det.locations_id, loc.name, old_1.in_camea_contract, loc.lat, loc.lng, loc.route, det.directions_id, dir.name, (
        CASE
            WHEN (det.directions_id::text IN ( SELECT min(bicyclecounters_detections.directions_id::text) AS min
               FROM bicycle_counters.bicyclecounters_detections
              GROUP BY bicyclecounters_detections.locations_id)) THEN 'Směr 1'::text
            ELSE 'Směr 2'::text
        END);

-- bikecounter history table

CREATE OR REPLACE VIEW analytic.v_camea_bikecounters_history_table
AS SELECT t1.datum,
    t1.locations_id,
    t1.location_name,
    t1.route,
    t1.direction_name,
    t1.in_camea_contract,
    t1.dir1det,
        CASE
            WHEN t2.direction_name::text = t1.direction_name::text THEN 'není'::character varying
            ELSE t2.direction_name
        END AS direction2_name,
    t2.dir2det
   FROM ( SELECT to_timestamp((det.measured_from / 1000)::double precision)::date AS datum,
            det.locations_id,
            loc.name AS location_name,
            replace(loc.route::text, ' '::text, ''::text) AS route,
            dir.name AS direction_name,
            old_1.in_camea_contract,
                CASE
                    WHEN sum(det.value) IS NULL THEN 0::bigint
                    ELSE sum(det.value)
                END AS dir1det
           FROM bicycle_counters.bicyclecounters_detections det
             LEFT JOIN bicyclecounters_locations loc ON det.locations_id::text = loc.id::text
             LEFT JOIN bicyclecounters_directions dir ON det.directions_id::text = dir.id::text
             LEFT JOIN analytic.camea_bikecounters_in_contract old_1 ON loc.vendor_id::text = old_1.location_id::text
          WHERE (det.directions_id::text IN ( SELECT min(bicyclecounters_directions.id::text) AS min
                   FROM bicyclecounters_directions
                  GROUP BY bicyclecounters_directions.locations_id))
          GROUP BY (to_timestamp((det.measured_from / 1000)::double precision)::date), det.locations_id, loc.name, (replace(loc.route::text, ' '::text, ''::text)), dir.name, old_1.in_camea_contract) t1
     LEFT JOIN ( SELECT to_timestamp((det2.measured_from / 1000)::double precision)::date AS datum,
            det2.locations_id,
            loc.name AS location_name,
            dir.vendor_id AS direction_id,
            dir.name AS direction_name,
                CASE
                    WHEN sum(det2.value) IS NULL THEN 0::bigint
                    ELSE sum(det2.value)
                END AS dir2det
           FROM bicycle_counters.bicyclecounters_detections det2
             LEFT JOIN bicyclecounters_locations loc ON det2.locations_id::text = loc.id::text
             LEFT JOIN bicyclecounters_directions dir ON det2.directions_id::text = dir.id::text
          WHERE (det2.directions_id::text IN ( SELECT max(bicyclecounters_directions.id::text) AS max
                   FROM bicyclecounters_directions
                  GROUP BY bicyclecounters_directions.locations_id))
          GROUP BY (to_timestamp((det2.measured_from / 1000)::double precision)::date), det2.locations_id, loc.name, dir.vendor_id, dir.name) t2 ON t1.datum = t2.datum AND t1.locations_id::text = t2.locations_id::text;

-- camea data month

CREATE OR REPLACE VIEW v_camea_bikecounters_data_month
AS SELECT d1.date_time,
    d1.date_hour,
    d1.locations_id,
    d1.location_name,
    d1.directions_id AS smer_1,
    d2.directions_id AS smer_2,
    d1.direction_name AS dir1_name,
    d2.direction_name AS dir2_name,
    d1.dir1det,
    d2.dir2det
   FROM ( SELECT to_timestamp((det.measured_from / 1000)::double precision) AS date_time,
            date_trunc('hour'::text, to_timestamp((det.measured_from / 1000)::double precision)) AS date_hour,
            det.locations_id,
            loc.name AS location_name,
            det.directions_id,
            dir.name AS direction_name,
            det.value AS dir1det
           FROM bicyclecounters_detections det
             LEFT JOIN bicyclecounters_locations loc ON det.locations_id::text = loc.id::text
             LEFT JOIN bicyclecounters_directions dir ON det.directions_id::text = dir.id::text
          WHERE (det.directions_id::text IN ( SELECT max(bicyclecounters_directions.id::text) AS max --vyberu smer 1
                   FROM bicyclecounters_directions
                  GROUP BY bicyclecounters_directions.locations_id)) AND det.measured_from::double precision >= (date_part('epoch'::text, now() - '1 mon'::interval) * 1000::double precision)) d1
     FULL JOIN ( SELECT to_timestamp((det2.measured_from / 1000)::double precision) AS date_time,
            det2.locations_id,
            loc.name AS location_name,
            det2.directions_id,
            dir.name AS direction_name,
            det2.value AS dir2det
           FROM bicyclecounters_detections det2
             LEFT JOIN bicyclecounters_locations loc ON det2.locations_id::text = loc.id::text
             LEFT JOIN bicyclecounters_directions dir ON det2.directions_id::text = dir.id::text
          WHERE (det2.directions_id::text IN ( SELECT min(bicyclecounters_directions.id::text) AS min -- vyberu smer 2
                   FROM bicyclecounters_directions
                  GROUP BY bicyclecounters_directions.locations_id)) AND det2.measured_from::double precision >= (date_part('epoch'::text, now() - '1 mon'::interval) * 1000::double precision)) d2 ON d1.date_time = d2.date_time AND d1.locations_id::text = d2.locations_id::text;

