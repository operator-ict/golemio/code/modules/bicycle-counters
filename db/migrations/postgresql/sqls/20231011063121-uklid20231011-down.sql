-- analytic.camea_bikecounters_in_contract definition

CREATE TABLE analytic.camea_bikecounters_in_contract (
	id int4 NULL,
	location_id varchar(50) NULL,
	location_name varchar(50) NULL,
	route_name varchar(50) NULL,
	lat numeric NULL,
	lng numeric NULL,
	in_camea_contract bool NULL,
	old_id varchar(50) NULL
);