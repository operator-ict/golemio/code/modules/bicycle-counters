CREATE OR REPLACE VIEW v_camea_bikecounters_data_month
AS SELECT d1.date_time,
    d1.date_hour,
    d1.locations_id,
    d1.location_name,
    d1.directions_id AS smer_1,
    d2.directions_id AS smer_2,
    d1.direction_name AS dir1_name,
    d2.direction_name AS dir2_name,
    d1.dir1det,
    d2.dir2det
   FROM ( SELECT to_timestamp((det.measured_from / 1000)::double precision) AS date_time,
            date_trunc('hour'::text, to_timestamp((det.measured_from / 1000)::double precision)) AS date_hour,
            det.locations_id,
            loc.name AS location_name,
            det.directions_id,
            dir.name AS direction_name,
            det.value AS dir1det
           FROM bicyclecounters_detections det
             LEFT JOIN bicyclecounters_locations loc ON det.locations_id::text = loc.id::text
             LEFT JOIN bicyclecounters_directions dir ON det.directions_id::text = dir.id::text
          WHERE (det.directions_id::text IN ( SELECT max(bicyclecounters_directions.id::text) AS max --vyberu smer 1
                   FROM bicyclecounters_directions
                  GROUP BY bicyclecounters_directions.locations_id)) AND det.measured_from::double precision >= (date_part('epoch'::text, now() - '1 mon'::interval) * 1000::double precision)) d1
     FULL JOIN ( SELECT to_timestamp((det2.measured_from / 1000)::double precision) AS date_time,
            det2.locations_id,
            loc.name AS location_name,
            det2.directions_id,
            dir.name AS direction_name,
            det2.value AS dir2det
           FROM bicyclecounters_detections det2
             LEFT JOIN bicyclecounters_locations loc ON det2.locations_id::text = loc.id::text
             LEFT JOIN bicyclecounters_directions dir ON det2.directions_id::text = dir.id::text
          WHERE (det2.directions_id::text IN ( SELECT min(bicyclecounters_directions.id::text) AS min -- vyberu smer 2
                   FROM bicyclecounters_directions
                  GROUP BY bicyclecounters_directions.locations_id)) AND det2.measured_from::double precision >= (date_part('epoch'::text, now() - '1 mon'::interval) * 1000::double precision)) d2 ON d1.date_time = d2.date_time AND d1.locations_id::text = d2.locations_id::text;

