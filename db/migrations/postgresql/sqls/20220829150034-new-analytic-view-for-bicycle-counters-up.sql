CREATE OR REPLACE VIEW analytic.v_camea_bikecounters_history
AS SELECT to_timestamp((det.measured_from / 1000)::double precision)::date AS datum,
    det.locations_id,
    loc.name AS location_name,
    old_1.in_camea_contract,
    loc.lat AS latitude,
    loc.lng AS longitude,
    replace(loc.route::text, ' '::text, ''::text) AS route,
    det.directions_id,
    dir.name AS direction_name,
        CASE
            WHEN (det.directions_id::text IN ( SELECT min(bicyclecounters_detections.directions_id::text) AS min
               FROM bicycle_counters.bicyclecounters_detections
              GROUP BY bicyclecounters_detections.locations_id)) THEN 'Směr 1'::text
            ELSE 'Směr 2'::text
        END AS direction_num,
        CASE
            WHEN sum(det.value) IS NULL THEN 0::bigint
            ELSE sum(det.value)
        END AS detections
   FROM bicycle_counters.bicyclecounters_detections det
     LEFT JOIN bicycle_counters.bicyclecounters_locations loc ON det.locations_id::text = loc.id::text
     LEFT JOIN bicycle_counters.bicyclecounters_directions dir ON det.directions_id::text = dir.id::text
     LEFT JOIN analytic.camea_bikecounters_in_contract old_1 ON loc.vendor_id::text = old_1.location_id::text
  GROUP BY (to_timestamp((det.measured_from / 1000)::double precision)::date), det.locations_id, loc.name, old_1.in_camea_contract, loc.lat, loc.lng, loc.route, det.directions_id, dir.name, (
        CASE
            WHEN (det.directions_id::text IN ( SELECT min(bicyclecounters_detections.directions_id::text) AS min
               FROM bicycle_counters.bicyclecounters_detections
              GROUP BY bicyclecounters_detections.locations_id)) THEN 'Směr 1'::text
            ELSE 'Směr 2'::text
        END);

CREATE OR REPLACE VIEW analytic.v_camea_bikecounters_history_table
AS SELECT t1.datum,
    t1.locations_id,
    t1.location_name,
    t1.route,
    t1.direction_name,
    t1.in_camea_contract,
    t1.dir1det,
        CASE
            WHEN t2.direction_name::text = t1.direction_name::text THEN 'není'::character varying
            ELSE t2.direction_name
        END AS direction2_name,
    t2.dir2det
   FROM ( SELECT to_timestamp((det.measured_from / 1000)::double precision)::date AS datum,
            det.locations_id,
            loc.name AS location_name,
            replace(loc.route::text, ' '::text, ''::text) AS route,
            dir.name AS direction_name,
            old_1.in_camea_contract,
                CASE
                    WHEN sum(det.value) IS NULL THEN 0::bigint
                    ELSE sum(det.value)
                END AS dir1det
           FROM bicycle_counters.bicyclecounters_detections det
             LEFT JOIN bicyclecounters_locations loc ON det.locations_id::text = loc.id::text
             LEFT JOIN bicyclecounters_directions dir ON det.directions_id::text = dir.id::text
             LEFT JOIN analytic.camea_bikecounters_in_contract old_1 ON loc.vendor_id::text = old_1.location_id::text
          WHERE (det.directions_id::text IN ( SELECT min(bicyclecounters_directions.id::text) AS min
                   FROM bicyclecounters_directions
                  GROUP BY bicyclecounters_directions.locations_id))
          GROUP BY (to_timestamp((det.measured_from / 1000)::double precision)::date), det.locations_id, loc.name, (replace(loc.route::text, ' '::text, ''::text)), dir.name, old_1.in_camea_contract) t1
     LEFT JOIN ( SELECT to_timestamp((det2.measured_from / 1000)::double precision)::date AS datum,
            det2.locations_id,
            loc.name AS location_name,
            dir.vendor_id AS direction_id,
            dir.name AS direction_name,
                CASE
                    WHEN sum(det2.value) IS NULL THEN 0::bigint
                    ELSE sum(det2.value)
                END AS dir2det
           FROM bicycle_counters.bicyclecounters_detections det2
             LEFT JOIN bicyclecounters_locations loc ON det2.locations_id::text = loc.id::text
             LEFT JOIN bicyclecounters_directions dir ON det2.directions_id::text = dir.id::text
          WHERE (det2.directions_id::text IN ( SELECT max(bicyclecounters_directions.id::text) AS max
                   FROM bicyclecounters_directions
                  GROUP BY bicyclecounters_directions.locations_id))
          GROUP BY (to_timestamp((det2.measured_from / 1000)::double precision)::date), det2.locations_id, loc.name, dir.vendor_id, dir.name) t2 ON t1.datum = t2.datum AND t1.locations_id::text = t2.locations_id::text;
