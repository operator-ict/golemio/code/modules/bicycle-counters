CREATE OR REPLACE VIEW analytic.v_bicyclecounters_24h_missing
AS SELECT max(to_timestamp((bd.measured_from / 1000)::double precision)) AS last_refresh,
    bl.name
   FROM bicyclecounters_detections bd
     JOIN bicyclecounters_locations bl ON bd.locations_id::text = bl.id::text
  GROUP BY bl.name
 HAVING max(to_timestamp((bd.measured_from / 1000)::double precision)) < (now() - '1 day'::interval) AND max(to_timestamp((bd.measured_from / 1000)::double precision)) >= (CURRENT_DATE - '3 mons'::interval);

COMMENT ON VIEW analytic.v_bicyclecounters_24h_missing IS 'alerting for Martin Lér';
