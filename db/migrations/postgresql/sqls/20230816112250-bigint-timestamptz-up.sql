/* Replace with your SQL commands */

-- drop existing analytic views connected to tables we change
DROP VIEW IF EXISTS  analytic.v_camea_bikecounters_24h_missing;
DROP VIEW IF EXISTS analytic.v_camea_bikecounters_24h_missing;
DROP VIEW IF EXISTS analytic.v_camea_bikecounters_all_data_history;
DROP VIEW IF EXISTS analytic.v_camea_bikecounters_all_data_table_today;
DROP VIEW IF EXISTS analytic.v_camea_bikecounters_all_data_today;
DROP VIEW IF EXISTS analytic.v_camea_bikecounters_data_disbalance;
DROP VIEW IF EXISTS analytic.v_camea_bikecounters_data_quality;
DROP VIEW IF EXISTS analytic.v_camea_bikecounters_last_update;
DROP VIEW IF EXISTS analytic.v_camea_bikecounters_name_table;
DROP VIEW IF EXISTS analytic.v_camea_data_disbalance;
DROP VIEW IF EXISTS analytic.v_camea_data_quality;
DROP VIEW IF EXISTS analytic.v_camea_last_update;

DROP VIEW IF EXISTS analytic.v_camea_bikecounters_history;
DROP VIEW IF EXISTS analytic.v_camea_bikecounters_history_table;
DROP VIEW IF EXISTS analytic.v_bicyclecounters_24h_missing;

-- drop only view in bicycle_counters schema
DROP VIEW IF EXISTS v_camea_bikecounters_data_month;

--rename original table
ALTER TABLE bicyclecounters_detections RENAME TO bicyclecounters_detections_old;

--rename back partitions 
ALTER TABLE bicyclecounters_detections_min RENAME TO bicyclecounters_detections_min_old;
ALTER TABLE bicyclecounters_detections_y2019 RENAME TO bicyclecounters_detections_y2019_old;
ALTER TABLE bicyclecounters_detections_y2020 RENAME TO bicyclecounters_detections_y2020_old;
ALTER TABLE bicyclecounters_detections_y2021 RENAME TO bicyclecounters_detections_y2021_old;
ALTER TABLE bicyclecounters_detections_y2022 RENAME TO bicyclecounters_detections_y2022_old;
ALTER TABLE bicyclecounters_detections_y2023 RENAME TO bicyclecounters_detections_y2023_old;
ALTER TABLE bicyclecounters_detections_y2024 RENAME TO bicyclecounters_detections_y2024_old;
ALTER TABLE bicyclecounters_detections_y2025 RENAME TO bicyclecounters_detections_y2025_old;
ALTER TABLE bicyclecounters_detections_y2026 RENAME TO bicyclecounters_detections_y2026_old;
ALTER TABLE bicyclecounters_detections_y2027up RENAME TO bicyclecounters_detections_y2027up_old;

-- CREATE TABLE bicyclecounters_detections 
CREATE TABLE bicyclecounters_detections (
	locations_id varchar(255) NOT NULL,
	directions_id varchar(255) NOT NULL,
	measured_from timestamptz NOT NULL,
	measured_to timestamptz NOT NULL,
	value int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	value_pedestrians int4 NULL,
	CONSTRAINT bicyclecounters_detections_pkey_new PRIMARY KEY (locations_id, directions_id, measured_from)
)
PARTITION BY RANGE (measured_from);
;

-- create indices

CREATE INDEX bicyclecounters_detections_directions_idx ON bicyclecounters_detections USING btree (directions_id);
CREATE INDEX bicyclecounters_detections_locations_idx ON bicyclecounters_detections USING btree (locations_id);


-- create partitions in the new table
CREATE TABLE bicyclecounters_detections_min PARTITION OF bicyclecounters_detections FOR VALUES FROM (MINVALUE) TO  ('2019-01-01');
CREATE TABLE bicyclecounters_detections_y2019 PARTITION OF bicyclecounters_detections FOR VALUES FROM ('2019-01-01') TO ('2020-01-01');
CREATE TABLE bicyclecounters_detections_y2020 PARTITION OF bicyclecounters_detections FOR VALUES FROM ('2020-01-01') TO ('2021-01-01');
CREATE TABLE bicyclecounters_detections_y2021 PARTITION OF bicyclecounters_detections FOR VALUES FROM ('2021-01-01') TO ('2022-01-01');
CREATE TABLE bicyclecounters_detections_y2022 PARTITION OF bicyclecounters_detections FOR VALUES FROM ('2022-01-01') TO ('2023-01-01');
CREATE TABLE bicyclecounters_detections_y2023 PARTITION OF bicyclecounters_detections FOR VALUES FROM ('2023-01-01') TO ('2024-01-01');
CREATE TABLE bicyclecounters_detections_y2024 PARTITION OF bicyclecounters_detections FOR VALUES FROM ('2024-01-01') TO ('2025-01-01');
CREATE TABLE bicyclecounters_detections_y2025 PARTITION OF bicyclecounters_detections FOR VALUES FROM ('2025-01-01') TO ('2026-01-01');
CREATE TABLE bicyclecounters_detections_y2026 PARTITION OF bicyclecounters_detections FOR VALUES FROM ('2026-01-01') TO ('2027-01-01');
CREATE TABLE bicyclecounters_detections_y2027up PARTITION OF bicyclecounters_detections FOR VALUES FROM ('2027-01-01') TO (maxvalue);

-- change name 
ALTER TABLE bicyclecounters_temperatures RENAME TO bicyclecounters_temperatures_old;

--rename partitions 
ALTER TABLE bicyclecounters_temperatures_min RENAME TO bicyclecounters_temperatures_min_old;
ALTER TABLE bicyclecounters_temperatures_y2019 RENAME TO bicyclecounters_temperatures_y2019_old;
ALTER TABLE bicyclecounters_temperatures_y2020 RENAME TO bicyclecounters_temperatures_y2020_old;
ALTER TABLE bicyclecounters_temperatures_y2021 RENAME TO bicyclecounters_temperatures_y2021_old;
ALTER TABLE bicyclecounters_temperatures_y2022 RENAME TO bicyclecounters_temperatures_y2022_old;
ALTER TABLE bicyclecounters_temperatures_y2023 RENAME TO bicyclecounters_temperatures_y2023_old;
ALTER TABLE bicyclecounters_temperatures_y2024 RENAME TO bicyclecounters_temperatures_y2024_old;
ALTER TABLE bicyclecounters_temperatures_y2025 RENAME TO bicyclecounters_temperatures_y2025_old;
ALTER TABLE bicyclecounters_temperatures_y2026 RENAME TO bicyclecounters_temperatures_y2026_old;
ALTER TABLE bicyclecounters_temperatures_y2027up RENAME TO bicyclecounters_temperatures_y2027up_old;

--create second table with partitions

CREATE TABLE bicyclecounters_temperatures (
	locations_id varchar(255) NOT NULL,
	measured_from timestamptz NOT NULL,
	measured_to timestamptz NOT NULL,
	value int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT bicyclecounters_temperatures_pkey_new PRIMARY KEY (locations_id, measured_from)
)
PARTITION BY RANGE (measured_from);

-- create index

CREATE INDEX bicyclecounters_temperatures_locations_idx ON bicyclecounters_temperatures USING btree (locations_id);

-- create partitions in the new table
CREATE TABLE bicyclecounters_temperatures_min PARTITION OF bicyclecounters_temperatures FOR VALUES FROM (MINVALUE) TO  ('2019-01-01');
CREATE TABLE bicyclecounters_temperatures_y2019 PARTITION OF bicyclecounters_temperatures FOR VALUES FROM ('2019-01-01') TO ('2020-01-01');
CREATE TABLE bicyclecounters_temperatures_y2020 PARTITION OF bicyclecounters_temperatures FOR VALUES FROM ('2020-01-01') TO ('2021-01-01');
CREATE TABLE bicyclecounters_temperatures_y2021 PARTITION OF bicyclecounters_temperatures FOR VALUES FROM ('2021-01-01') TO ('2022-01-01');
CREATE TABLE bicyclecounters_temperatures_y2022 PARTITION OF bicyclecounters_temperatures FOR VALUES FROM ('2022-01-01') TO ('2023-01-01');
CREATE TABLE bicyclecounters_temperatures_y2023 PARTITION OF bicyclecounters_temperatures FOR VALUES FROM ('2023-01-01') TO ('2024-01-01');
CREATE TABLE bicyclecounters_temperatures_y2024 PARTITION OF bicyclecounters_temperatures FOR VALUES FROM ('2024-01-01') TO ('2025-01-01');
CREATE TABLE bicyclecounters_temperatures_y2025 PARTITION OF bicyclecounters_temperatures FOR VALUES FROM ('2025-01-01') TO ('2026-01-01');
CREATE TABLE bicyclecounters_temperatures_y2026 PARTITION OF bicyclecounters_temperatures FOR VALUES FROM ('2026-01-01') TO ('2027-01-01');
CREATE TABLE bicyclecounters_temperatures_y2027up PARTITION OF bicyclecounters_temperatures FOR VALUES FROM ('2027-01-01') TO (maxvalue);
