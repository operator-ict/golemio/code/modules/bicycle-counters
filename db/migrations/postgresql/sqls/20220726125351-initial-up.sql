-- bicyclecounters_detections definition

CREATE TABLE bicyclecounters_detections (
	locations_id varchar(255) NOT NULL,
	directions_id varchar(255) NOT NULL,
	measured_from int8 NOT NULL,
	measured_to int8 NOT NULL,
	value int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	value_pedestrians int4 NULL,
	CONSTRAINT bicyclecounters_detections_pkey PRIMARY KEY (locations_id, directions_id, measured_from)
)
PARTITION BY RANGE (measured_from);
;
CREATE INDEX bicyclecounters_detections_directions_id ON bicyclecounters_detections USING btree (directions_id);
CREATE INDEX bicyclecounters_detections_locations_id ON bicyclecounters_detections USING btree (locations_id);

-- partitions
CREATE TABLE bicyclecounters_detections_min PARTITION OF bicyclecounters_detections FOR VALUES FROM (MINVALUE) TO (extract ('epoch' from '2019-01-01'::timestamp)*1000);
CREATE TABLE bicyclecounters_detections_y2019 PARTITION OF bicyclecounters_detections FOR VALUES FROM (extract ('epoch' from '2019-01-01'::timestamp)*1000) TO (extract ('epoch' from '2020-01-01'::timestamp)*1000);
CREATE TABLE bicyclecounters_detections_y2020 PARTITION OF bicyclecounters_detections FOR VALUES FROM (extract ('epoch' from '2020-01-01'::timestamp)*1000) TO (extract ('epoch' from '2021-01-01'::timestamp)*1000);
CREATE TABLE bicyclecounters_detections_y2021 PARTITION OF bicyclecounters_detections FOR VALUES FROM (extract ('epoch' from '2021-01-01'::timestamp)*1000) TO (extract ('epoch' from '2022-01-01'::timestamp)*1000);
CREATE TABLE bicyclecounters_detections_y2022 PARTITION OF bicyclecounters_detections FOR VALUES FROM (extract ('epoch' from '2022-01-01'::timestamp)*1000) TO (extract ('epoch' from '2023-01-01'::timestamp)*1000);
CREATE TABLE bicyclecounters_detections_y2023 PARTITION OF bicyclecounters_detections FOR VALUES FROM (extract ('epoch' from '2023-01-01'::timestamp)*1000) TO (extract ('epoch' from '2024-01-01'::timestamp)*1000);
CREATE TABLE bicyclecounters_detections_y2024 PARTITION OF bicyclecounters_detections FOR VALUES FROM (extract ('epoch' from '2024-01-01'::timestamp)*1000) TO (extract ('epoch' from '2025-01-01'::timestamp)*1000);
CREATE TABLE bicyclecounters_detections_y2025 PARTITION OF bicyclecounters_detections FOR VALUES FROM (extract ('epoch' from '2025-01-01'::timestamp)*1000) TO (extract ('epoch' from '2026-01-01'::timestamp)*1000);
CREATE TABLE bicyclecounters_detections_y2026 PARTITION OF bicyclecounters_detections FOR VALUES FROM (extract ('epoch' from '2026-01-01'::timestamp)*1000) TO (extract ('epoch' from '2027-01-01'::timestamp)*1000);

CREATE TABLE bicyclecounters_detections_y2027up PARTITION OF bicyclecounters_detections FOR VALUES FROM (extract ('epoch' from '2027-01-01'::timestamp)*1000) TO (maxvalue);


-- bicyclecounters_directions definition

CREATE TABLE bicyclecounters_directions (
	id varchar(255) NOT NULL,
	vendor_id varchar(255) NOT NULL,
	locations_id varchar(255) NOT NULL,
	"name" varchar(255) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT bicyclecounters_directions_pkey PRIMARY KEY (id)
);
CREATE INDEX bicyclecounters_directions_locations_id ON bicyclecounters_directions USING btree (locations_id);


-- bicyclecounters_locations definition

CREATE TABLE bicyclecounters_locations (
	id varchar(255) NOT NULL,
	vendor_id varchar(255) NOT NULL,
	lat float8 NOT NULL,
	lng float8 NOT NULL,
	"name" varchar(255) NULL,
	route varchar(255) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT bicyclecounters_locations_pkey PRIMARY KEY (id)
);


-- bicyclecounters_temperatures definition

CREATE TABLE bicyclecounters_temperatures (
	locations_id varchar(255) NOT NULL,
	measured_from int8 NOT NULL,
	measured_to int8 NOT NULL,
	value int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT bicyclecounters_temperatures_pkey PRIMARY KEY (locations_id, measured_from)
)
PARTITION BY RANGE (measured_from);

CREATE INDEX bicyclecounters_temperatures_locations_id ON bicyclecounters_temperatures USING btree (locations_id);

-- partitions
CREATE TABLE bicyclecounters_temperatures_min PARTITION OF bicyclecounters_temperatures FOR VALUES FROM (MINVALUE) TO (extract ('epoch' from '2019-01-01'::timestamp)*1000);
CREATE TABLE bicyclecounters_temperatures_y2019 PARTITION OF bicyclecounters_temperatures FOR VALUES FROM (extract ('epoch' from '2019-01-01'::timestamp)*1000) TO (extract ('epoch' from '2020-01-01'::timestamp)*1000);
CREATE TABLE bicyclecounters_temperatures_y2020 PARTITION OF bicyclecounters_temperatures FOR VALUES FROM (extract ('epoch' from '2020-01-01'::timestamp)*1000) TO (extract ('epoch' from '2021-01-01'::timestamp)*1000);
CREATE TABLE bicyclecounters_temperatures_y2021 PARTITION OF bicyclecounters_temperatures FOR VALUES FROM (extract ('epoch' from '2021-01-01'::timestamp)*1000) TO (extract ('epoch' from '2022-01-01'::timestamp)*1000);
CREATE TABLE bicyclecounters_temperatures_y2022 PARTITION OF bicyclecounters_temperatures FOR VALUES FROM (extract ('epoch' from '2022-01-01'::timestamp)*1000) TO (extract ('epoch' from '2023-01-01'::timestamp)*1000);
CREATE TABLE bicyclecounters_temperatures_y2023 PARTITION OF bicyclecounters_temperatures FOR VALUES FROM (extract ('epoch' from '2023-01-01'::timestamp)*1000) TO (extract ('epoch' from '2024-01-01'::timestamp)*1000);
CREATE TABLE bicyclecounters_temperatures_y2024 PARTITION OF bicyclecounters_temperatures FOR VALUES FROM (extract ('epoch' from '2024-01-01'::timestamp)*1000) TO (extract ('epoch' from '2025-01-01'::timestamp)*1000);
CREATE TABLE bicyclecounters_temperatures_y2025 PARTITION OF bicyclecounters_temperatures FOR VALUES FROM (extract ('epoch' from '2025-01-01'::timestamp)*1000) TO (extract ('epoch' from '2026-01-01'::timestamp)*1000);
CREATE TABLE bicyclecounters_temperatures_y2026 PARTITION OF bicyclecounters_temperatures FOR VALUES FROM (extract ('epoch' from '2026-01-01'::timestamp)*1000) TO (extract ('epoch' from '2027-01-01'::timestamp)*1000);

CREATE TABLE bicyclecounters_temperatures_y2027up PARTITION OF bicyclecounters_temperatures FOR VALUES FROM (extract ('epoch' from '2027-01-01'::timestamp)*1000) TO (maxvalue);