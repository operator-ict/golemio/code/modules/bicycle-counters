# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.3.4] - 2024-10-24

### Fixed

-   asyncAPI prefixes

## [1.3.3] - 2024-10-22

### Added

-   asyncAPI docs file ([integration-engine#261](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/261))

## [1.3.2] - 2024-09-12

### Changed

-   `.gitlab-ci.yml` cleanup ([devops#320](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/issues/320))

## [1.3.1] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.3.0] - 2024-06-03

### Added

-   add cache-control header to all responses ([core#100](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/100))

### Removed

-   remove redis useCacheMiddleware ([core#100](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/100))

## [1.2.13] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.2.12] - 2024-04-08

### Changed

-   axios exchanged for native fetch ([core#99](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/99))

## [1.2.11] - 2024-03-25

### Fixed

-   Fixed router query validation rules ([core#93](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/93))

## [1.2.10] - 2024-01-04

### Fixed

-   optimalization of filtering for detection endpoint ([incident link](https://gitlab.com/operator-ict/golemio/code/general/-/issues/538/))

## [1.2.9] - 2023-10-16

### Removed

-   odstranění tabulky analytic.camea_bikecounters_in_contract;

## [1.2.8] - 2023-09-27

### Changed

-   API versioning - version moved to path ([core#80](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/80))

## [1.2.7] - 2023-08-28

### Changed

-   Replacing bigint with timestamptz ([bicycle-counters#9](https://gitlab.com/operator-ict/golemio/code/modules/bicycle-counters/-/issues/9))

## [1.2.6] - 2023-07-31

### Changed

-   Remove momentJS and replace it with DateTime wrapper '[core#68](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/68))

## [1.2.5] - 2023-06-28

### Changed

-   refactoring worker to separate tasks ([bicycle-counters#8](https://gitlab.com/operator-ict/golemio/code/modules/bicycle-counters/-/issues/8))

## [1.2.4] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.2.3] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [1.2.2] - 2023-05-29

### Changed

-   Update openapi docs

## [1.2.1] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.1] - 2023-02-20

### Added

-   new view v_camea_bikecounters_data_month (https://gitlab.com/operator-ict/golemio/projekty/oict/d0005-cykloscitace/-/issues/59)

## [1.1.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.0.12] - 2022-11-29

### Changed

-   Update gitlab-ci template

## [1.0.11] - 2022-09-06

### Changed

-   Dashboard update ([d0005-cykloscitace#50](https://gitlab.com/operator-ict/golemio/projekty/oict/d0005-cykloscitace/-/issues/50))

## [1.0.10] - 2022-08-24

### Changed

-   DB Schema separation ([bicycle-counters#5](https://gitlab.com/operator-ict/golemio/code/modules/bicycle-counters/-/issues/5))

### Removed

-   The Lodash library was replaced with js functions ([core#42](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/42))

## [1.0.9] - 2022-07-18

### Changed

-   @golemio/eslint-config version update from 1.0.2 to 1.1.0

### Added

-   !apidoc to npm.ignore [OG#79](https://gitlab.com/operator-ict/golemio/code/input-gateway/-/issues/79)

## [1.0.8] - 2022-06-07

### Changed

-   Typescript version update from 4.4.4 to 4.6.4

### Added

-   Implementation docs

## [1.0.7] - 2022-05-18

### Fixed

-   Fixed JSON validation schema

## [1.0.6] - 2022-05-02

### Changed

-   Temperature data type

## [1.0.5] - 2022-03-31

### Fixed

-   Bug - nulls in detections were saved in database as 0. Now detection value null is saved as NULL in db. [bicycle-counters#6](https://gitlab.com/operator-ict/golemio/code/modules/bicycle-counters/-/issues/6)

## [1.0.4] - 2021-12-06

### Changed

-   Mongoose validator to JSON validator ([bicycle-counters#3](https://gitlab.com/operator-ict/golemio/code/modules/bicycle-counters/-/issues/3))
