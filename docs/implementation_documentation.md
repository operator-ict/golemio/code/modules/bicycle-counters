# Implementační dokumentace modulu *bicycle-counters*

## Záměr

Modul slouží k ukládání informací o sčítačích kol v Praze.

Dostupní poskytovatelé:
- Camea
- EcoCounter


## Vstupní data

### Data aktivně stahujeme

Měření Camea senzorů jsou validovány a přepočítány každý den o 05:00 (UTC) a můžou být zpětně změněna.
Data z Camea senzorů obsahují informaci o teplotě.

#### *Camea*

##### Lokalita

- zdroj dat
  - url: `config.datasources.BicycleCountersCamea`
  - parametry dotazu: žádné
- formát dat
  - protokol: http
  - datový typ: json
  - [validační schéma: datasourceCameaJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/bicycle-counters/-/blob/development/src/schema-definitions/index.ts)
  - [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/bicycle-counters/-/blob/development/test/integration-engine/data/bicyclecounters-camea-datasource.json)
- frekvence stahování
  - cron definice:
    - `cron.dataplatform.bicyclecounters.refreshCameaDataLastXHoursInDB` (`0 */5 * * * *`)
    - `cron.dataplatform.bicyclecounters.refreshCameaDataPreviousDayInDB` (`0 0 5 * * *`)
- název rabbitmq fronty
  - `dataplatform.bicyclecounters.refreshCameaDataLastXHoursInDB`
  - `dataplatform.bicyclecounters.refreshCameaDataPreviousDayInDB`
  - `dataplatform.bicyclecounters.refreshCameaDataSpecificDayInDB`

##### Měření

- zdroj dat
    - url: `config.datasources.BicycleCountersCameaMeasurements`
    - parametry dotazu: žádné
- formát dat
    - protokol: http
    - datový typ: json
    - [validační schéma: datasourceCameaMeasurementsJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/bicycle-counters/-/blob/development/src/schema-definitions/index.ts)
    - [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/bicycle-counters/-/blob/development/test/integration-engine/data/bicyclecounters-camea-measurements-datasource.json)
- frekvence stahování
    - cron definice:
        - stahuje se spolu s lokalitou
- název rabbitmq fronty
    - `dataplatform.bicyclecounters.updateCamea`

#### *EcoCounter*

##### Lokalita

- zdroj dat
    - url: `config.datasources.BicycleCountersEcoCounter`
    - parametry dotazu: Bearer `config.datasources.CountersEcoCounterTokens.PRAHA`
- formát dat
    - protokol: http
    - datový typ: json
    - [validační schéma: datasourceEcoCounterJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/bicycle-counters/-/blob/development/src/schema-definitions/index.ts)
    - [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/bicycle-counters/-/blob/development/test/integration-engine/data/bicyclecounters-ecocounter-datasource.json)
- frekvence stahování
    - cron definice:
        - `cron.dataplatform.bicyclecounters.refreshEcoCounterDataInDB` (`0 */5 * * * *`)
- název rabbitmq fronty
    - `dataplatform.bicyclecounters.refreshEcoCounterDataInDB`

##### Měření

- zdroj dat
    - url: `config.datasources.BicycleCountersEcoCounterMeasurements`
    - parametry dotazu: Bearer `config.datasources.CountersEcoCounterTokens.PRAHA`
- formát dat
    - protokol: http
    - datový typ: json
    - [validační schéma: datasourceEcoCounterMeasurementsJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/bicycle-counters/-/blob/development/src/schema-definitions/index.ts)
    - [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/bicycle-counters/-/blob/development/test/integration-engine/data/bicyclecounters-ecocounter-measurements-datasource.json)
- frekvence stahování
    - cron definice:
        - stahuje se spolu s lokalitou
- název rabbitmq fronty
    - `dataplatform.bicyclecounters.updateEcoCounter`

## Zpracování dat / transformace

Popis transformace a případného obohacení dat. Zaměřeno hlavně na workery a jejich metody.

### Worker BicycleCountersWorker

#### refreshCameaDataLastXHoursInDB

- vstupní rabbitmq fronta
  - `dataplatform.bicyclecounters.refreshCameaDataLastXHoursInDB`
  - bez parametrů
- závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
  - `dataplatform.bicyclecounters.updateCamea`
  - parametry:
  ```
  { id: <vendor_id>, duration: CameaRefreshDurations.last3Hours }
  ```
- datové zdroje
  - dataSourceCamea
- transformace
  - [CameaTransformation](https://gitlab.com/operator-ict/golemio/code/modules/bicycle-counters/-/blob/development/src/integration-engine/CameaTransformation.ts) - mapování pro `locationsModel` a `directionsModel`
- data modely
  - locationsModel -> (schéma public) `bicyclecounters_locations`
  - directionsModel -> (schéma public) `bicyclecounters_directions`

#### refreshCameaDataPreviousDayInDB

- vstupní rabbitmq fronta
    - `dataplatform.bicyclecounters.refreshCameaDataPreviousDayInDB`
    - bez parametrů
- závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    - `dataplatform.bicyclecounters.updateCamea`
    - parametry:
  ```
  { id: <vendor_id>, duration: CameaRefreshDurations.previousDay }
  ```
- datové zdroje/transformace/data modely stejně jako `refreshCameaDataLastXHoursInDB`

#### refreshCameaDataSpecificDayInDB

bez cronu, k zpětné opravě dat

- vstupní rabbitmq fronta
    - `dataplatform.bicyclecounters.refreshCameaDataSpecificDayInDB`
    - bez parametrů
- závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    - `dataplatform.bicyclecounters.updateCamea`
    - parametry:
  ```
  { id: <vendor_id>, duration: CameaRefreshDurations.specificDay, date: <date, f.e. 2020-12-14> }
  ```
- datové zdroje/transformace/data modely stejně jako `refreshCameaDataLastXHoursInDB`

#### updateCamea

- vstupní rabbitmq fronta
    - `dataplatform.bicyclecounters.updateCamea`
    - parametry:
  ```
  { id: <vendor_id>, duration: CameaRefreshDurations.specificDay, date: <date, f.e. 2020-12-14> }
  ```
- datové zdroje
    - dataSourceCameaMeasurements
- transformace
    - [CameaMeasurementsTransformation](https://gitlab.com/operator-ict/golemio/code/modules/bicycle-counters/-/blob/development/src/integration-engine/https://gitlab.com/operator-ict/golemio/code/modules/bicycle-counters/-/blob/development/src/integration-engine/CameaMeasurementsTransformation.ts) mapování pro `detectionsModel` a `temperaturesModel`
- data modely
    - detectionsModel -> (schéma public) `bicyclecounters_detections`
    - temperaturesModel -> (schéma public) `bicyclecounters_temperatures`

#### refreshEcoCounterDataInDB

- vstupní rabbitmq fronta
    - `dataplatform.bicyclecounters.refreshEcoCounterDataInDB`
    - bez parametrů
- závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    - `dataplatform.bicyclecounters.updateEcoCounter`
    - parametry:
  ```
  { category: "bicycle", directions_id: <id>, id: <vendor_id>, locations_id: <locations_id> }
  ```
- datové zdroje
    - dataSourceEcoCounter
- transformace
    - [EcoCounterTransformation](https://gitlab.com/operator-ict/golemio/code/modules/bicycle-counters/-/blob/development/src/integration-engine/EcoCounterTransformation.ts) - mapování pro `locationsModel` a `directionsModel`
- data modely
    - locationsModel -> (schéma public) `bicyclecounters_locations`
    - directionsModel -> (schéma public) `bicyclecounters_directions`

#### updateEcoCounter

- vstupní rabbitmq fronta
    - `dataplatform.bicyclecounters.updateEcoCounter`
  - parametry:
  ```
  { category: "bicycle", directions_id: <id>, id: <vendor_id>, locations_id: <locations_id> }
  ```
- datové zdroje
    - dataSourceEcoCounterMeasurements
- transformace
    - [EcoCounterMeasurementsTransformation](https://gitlab.com/operator-ict/golemio/code/modules/bicycle-counters/-/blob/development/src/integration-engine/https://gitlab.com/operator-ict/golemio/code/modules/bicycle-counters/-/blob/development/src/integration-engine/EcoCounterMeasurementsTransformation.ts) mapování pro `detectionsModel` a `temperaturesModel`
- data modely
    - detectionsModel -> (schéma public) `bicyclecounters_detections`
    - temperaturesModel -> (schéma public) `bicyclecounters_temperatures`


## Uložení dat

Popis ukládání dat.

### Obecné

- typ databáze
  - PSQL
- databázové schéma
  - ![bicycle-counters er diagram](assets/bicycle-counters_erd.png)
- retence dat
  - `locationsModel` a `directionsModel` update
  - `detectionsModel` a `temperaturesModel` historizace bez promazávání

### *Název datového modelu*

- schéma a název tabulky / kolekce
- struktura tabulky / kolekce
  - odkaz na schéma, případně na migrační skripty
- ukázka dat


## Output API

### Obecné

- OpenAPI v3 dokumentace
  - [odkaz na dokumentaci](https://gitlab.com/operator-ict/golemio/code/modules/bicycle-counters/-/blob/development/docs/openapi.yaml)
- veřejné / neveřejné endpointy
  - api je veřejné
- postman kolekce
  - TBD

#### */bicyclecounters*

- zdrojové tabulky
  - `bicyclecounters_locations`
  - `bicyclecounters_directions`

#### */bicyclecounters/detections*

- zdrojové tabulky
  - `bicyclecounters_detections`

- (volitelně) nestandardní url a query parametry
  - `aggregate: boolean` umožňuje agregaci dle `directions_id`

#### */bicyclecounters/temperatures*

- zdrojové tabulky
  - `bicyclecounters_temperatures`

- (volitelně) nestandardní url a query parametry
  - `aggregate: boolean` umožňuje agregaci dle `locations_id`
