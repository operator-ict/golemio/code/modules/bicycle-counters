import Sequelize from "@golemio/core/dist/shared/sequelize";

// JsonSchema = JSON Schema Object for validations
// SDMA = Sequelize DefineModelAttributes

const datasourceCameaJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            bikecounter: { type: "string" },
            directions: {
                type: "array",
                items: {
                    type: "object",
                    properties: {
                        id: { type: "string" },
                        name: { type: "string" },
                    },
                    required: ["id", "name"],
                },
            },
            lat: { type: "number" },
            lon: { type: "number" },
            name: { type: "string" },
            route: { type: "string" },
        },
        required: ["bikecounter", "lat", "lon", "name", "route"],
    },
};

const datasourceCameaMeasurementsJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            bikecounter: { type: "string" },
            datetime: { type: "string" },
            detections: { type: ["number", "null"] },
            directions: {
                type: "array",
                items: {
                    type: "object",
                    properties: {
                        detections: { type: ["number", "null"] },
                        id: { type: "string" },
                        name: { type: "string" },
                        pedestrians: { type: "number" },
                    },
                    required: ["id", "name"],
                },
            },
            lat: { type: "number" },
            lon: { type: "number" },
            name: { type: "string" },
            pedestrians: { type: "number" },
            route: { type: "string" },
            temperature: { type: ["string", "null"] },
        },
    },
};

const datasourceEcoCounterJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            channels: {
                type: "array",
                items: {
                    type: "object",
                    properties: {
                        channels: { type: ["array", "null"] },
                        counter: { type: "string" },
                        domain: { type: "string" },
                        id: { type: "number" },
                        installationDate: { type: "string" },
                        interval: { type: "number" },
                        latitude: { type: "number" },
                        longitude: { type: "number" },
                        name: { type: "string" },
                        photos: { type: ["array", "null"] },
                        sens: { type: "number" },
                        timezone: { type: "string" },
                        userType: { type: "number" },
                    },
                    required: [
                        "counter",
                        "domain",
                        "id",
                        "installationDate",
                        "interval",
                        "latitude",
                        "longitude",
                        "name",
                        "sens",
                        "timezone",
                        "userType",
                    ],
                },
            },
            id: { type: "number" },
            interval: { type: "number" },
            latitude: { type: "number" },
            longitude: { type: "number" },
            name: { type: "string" },
        },
        required: ["id", "interval", "latitude", "longitude", "name"],
    },
};

const datasourceEcoCounterMeasurementsJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            counts: { type: ["number", "null"] },
            date: { type: "string" },
            status: { type: ["number", "null"] },
        },
        required: ["date"],
    },
};

// Locations
const outputLocationsSDMA: Sequelize.ModelAttributes<any> = {
    id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    lat: Sequelize.DECIMAL,
    lng: Sequelize.DECIMAL,
    name: Sequelize.STRING,
    route: Sequelize.STRING,
    vendor_id: Sequelize.STRING, // external id of location given by vendor API

    // Audit database fields
    create_batch_id: { type: Sequelize.BIGINT }, // ID of input batch
    created_at: { type: Sequelize.DATE }, // Time of insertion
    created_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of inserion
    update_batch_id: { type: Sequelize.BIGINT }, // ID last input batch which caused update
    updated_at: { type: Sequelize.DATE }, // Time of update
    updated_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of update
};

// only for Validator

const outputLocationsJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            id: { type: "string" },
            lat: { type: "number" },
            lng: { type: "number" },
            name: { type: "string" },
            route: { type: ["string", "null"] },
            vendor_id: { type: ["string", "number"] },
        },
        required: ["id", "lat", "lng", "vendor_id"],
    },
};

// Directions
const outputDirectionsSDMA: Sequelize.ModelAttributes<any> = {
    id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    locations_id: Sequelize.STRING, // foreign location id
    name: Sequelize.STRING,
    vendor_id: Sequelize.STRING, // external id of direction given by vendor API

    // Audit database fields
    create_batch_id: { type: Sequelize.BIGINT }, // ID of input batch
    created_at: { type: Sequelize.DATE }, // Time of insertion
    created_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of inserion
    update_batch_id: { type: Sequelize.BIGINT }, // ID last input batch which caused update
    updated_at: { type: Sequelize.DATE }, // Time of update
    updated_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of update
};

// only for Validator

const outputDirectionsJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            id: { type: "string" },
            locations_id: { type: "string" },
            name: { type: "string" },
            vendor_id: { type: ["string", "number"] },
        },
        required: ["id", "locations_id", "vendor_id"],
    },
};

// Detections
const outputDetectionsSDMA: Sequelize.ModelAttributes<any> = {
    directions_id: {
        // id, foreign direction id
        primaryKey: true,
        type: Sequelize.STRING,
    },
    locations_id: {
        // id, foreign location id
        primaryKey: true,
        type: Sequelize.STRING,
    },
    measured_from: {
        // id
        primaryKey: true,
        type: Sequelize.DATE,
    },
    measured_to: Sequelize.DATE,
    value: Sequelize.INTEGER,

    // Audit database fields
    create_batch_id: { type: Sequelize.BIGINT }, // ID of input batch
    created_at: { type: Sequelize.DATE }, // Time of insertion
    created_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of inserion
    update_batch_id: { type: Sequelize.BIGINT }, // ID last input batch which caused update
    updated_at: { type: Sequelize.DATE }, // Time of update
    updated_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of update
};

// only for Validator

const outputDetectionsJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            directions_id: { type: "string" },
            locations_id: { type: "string" },
            measured_from: { type: "string" },
            measured_to: { type: "string" },
            value: { type: ["number", "null"] },
        },
        required: ["directions_id", "locations_id", "measured_from", "measured_to"],
    },
};

// Temperatures
const outputTemperaturesSDMA: Sequelize.ModelAttributes<any> = {
    locations_id: {
        // id, foreign location id
        primaryKey: true,
        type: Sequelize.STRING,
    },
    measured_from: {
        // id
        primaryKey: true,
        type: Sequelize.DATE,
    },
    measured_to: Sequelize.DATE,
    value: Sequelize.INTEGER,

    // Audit database fields
    create_batch_id: { type: Sequelize.BIGINT }, // ID of input batch
    created_at: { type: Sequelize.DATE }, // Time of insertion
    created_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of inserion
    update_batch_id: { type: Sequelize.BIGINT }, // ID last input batch which caused update
    updated_at: { type: Sequelize.DATE }, // Time of update
    updated_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of update
};

// only for Validator

const outputTemperaturesJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            locations_id: { type: "string" },
            measured_from: { type: "string" },
            measured_to: { type: "string" },
            value: { type: ["number", "null"] },
        },
        required: ["locations_id", "measured_from", "measured_to", "value"],
    },
};

const forExport = {
    name: "BicycleCounters",
    pgSchema: "bicycle_counters",
    camea: {
        datasourceJsonSchema: datasourceCameaJsonSchema,
        measurementsDatasourceJsonSchema: datasourceCameaMeasurementsJsonSchema,
        name: "CameaBicycleCounters",
    },
    ecoCounter: {
        datasourceJsonSchema: datasourceEcoCounterJsonSchema,
        measurementsDatasourceJsonSchema: datasourceEcoCounterMeasurementsJsonSchema,
        name: "EcoCounterBicycleCounters",
    },

    detections: {
        name: "BicyclecountersDetections",
        outputJsonSchemaObject: outputDetectionsJsonSchema,
        outputSequelizeAttributes: outputDetectionsSDMA,
        pgTableName: "bicyclecounters_detections",
    },
    directions: {
        name: "BicyclecountersDirections",
        outputJsonSchemaObject: outputDirectionsJsonSchema,
        outputSequelizeAttributes: outputDirectionsSDMA,
        pgTableName: "bicyclecounters_directions",
    },

    locations: {
        name: "BicyclecountersLocations",
        outputJsonSchemaObject: outputLocationsJsonSchema,
        outputSequelizeAttributes: outputLocationsSDMA,
        pgTableName: "bicyclecounters_locations",
    },
    temperatures: {
        name: "BicyclecountersTemperatures",
        outputJsonSchemaObject: outputTemperaturesJsonSchema,
        outputSequelizeAttributes: outputTemperaturesSDMA,
        pgTableName: "bicyclecounters_temperatures",
    },
};

export { forExport as BicycleCounters };
