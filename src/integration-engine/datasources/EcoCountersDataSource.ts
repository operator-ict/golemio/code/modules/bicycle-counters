import { BicycleCounters } from "#sch/index";
import { config, DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class EcoCountersDataSource {
    public static getDataSource(): DataSource {
        return new DataSource(
            BicycleCounters.ecoCounter.name + "DataSource",
            new HTTPFetchProtocolStrategy({
                headers: {
                    Authorization: `Bearer ${config.datasources.CountersEcoCounterTokens.PRAHA}`,
                },
                method: "GET",
                url: config.datasources.BicycleCountersEcoCounter,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                BicycleCounters.ecoCounter.name + "DataSource",
                BicycleCounters.ecoCounter.datasourceJsonSchema
            )
        );
    }
}
