import { config, DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { BicycleCounters } from "#sch/index";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class EcoCounterMeasurementsDataSource {
    public static getDataSource(): DataSource {
        return new DataSource(
            BicycleCounters.ecoCounter.name + "MeasurementsDataSource",
            undefined as any,
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                BicycleCounters.ecoCounter.name + "MeasurementsDataSource",
                BicycleCounters.ecoCounter.measurementsDatasourceJsonSchema
            )
        );
    }
}
