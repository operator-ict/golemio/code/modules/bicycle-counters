import { BicycleCounters } from "#sch/index";
import { config, DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class CameaDataSource {
    public static getDataSource(): DataSource {
        return new DataSource(
            BicycleCounters.camea.name + "DataSource",
            new HTTPFetchProtocolStrategy({
                headers: {},
                method: "GET",
                url: config.datasources.BicycleCountersCamea,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(BicycleCounters.camea.name + "DataSource", BicycleCounters.camea.datasourceJsonSchema)
        );
    }
}
