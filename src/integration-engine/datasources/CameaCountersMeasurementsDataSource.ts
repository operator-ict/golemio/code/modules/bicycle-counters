import { config, DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { BicycleCounters } from "#sch/index";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class CameaCountersMeasurementsDataSource {
    public static getDataSource(): DataSource {
        return new DataSource(
            BicycleCounters.camea.name + "MeasurementsDataSource",
            undefined as any,
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                BicycleCounters.camea.name + "MeasurementsDataSource",
                BicycleCounters.camea.measurementsDatasourceJsonSchema
            )
        );
    }
}
