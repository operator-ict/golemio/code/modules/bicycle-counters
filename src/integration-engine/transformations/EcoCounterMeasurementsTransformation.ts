import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { BicycleCounters } from "#sch/index";
import { DateTime } from "@golemio/core/dist/helpers/DateTime";

export class EcoCounterMeasurementsTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = BicycleCounters.ecoCounter.name + "Measurements";
    }

    protected transformElement = async (element: any): Promise<any> => {
        // Repair UTC date, because EcoCounter API is actually working with local Europe/Prague time, not ISO!!!
        // Returned value 07:00:00+0000 is some hybrid between UTC time with offset 07:00:00+0100 and pure UTC
        // 06:00:00+0000
        const measuredFrom = DateTime.fromFormat(element.date.split("+")[0], "yyyy-LL-dd'T'TT", {
            timeZone: "Europe/Prague",
        });
        const measuredTo = measuredFrom.clone().add(15, "minutes");

        const res = {
            directions_id: null,
            locations_id: null,
            measured_from: measuredFrom.toISOString(),
            measured_to: measuredTo.toISOString(),
            value: element.counts,
        };

        return res;
    };
}
