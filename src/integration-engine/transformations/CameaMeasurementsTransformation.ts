import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { BicycleCounters } from "#sch/index";
import { DateTime } from "@golemio/core/dist/helpers/DateTime";
export class CameaMeasurementsTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = BicycleCounters.camea.name + "Measurements";
    }

    /**
     * Overrides BaseTransformation::transform
     */
    public transform = async (data: any | any[]): Promise<{ detections: any[]; temperatures: any[] }> => {
        const res: Record<string, any[]> = {
            detections: [],
            temperatures: [],
        };

        if (data instanceof Array) {
            const promises = data.map(async (element, i) => {
                const elemRes = await this.transformElement(element);
                if (elemRes) {
                    res.detections = res.detections.concat(elemRes.detections);
                    if (elemRes.temperature) {
                        res.temperatures.push(elemRes.temperature);
                    }
                }
                return;
            });
            await Promise.all(promises);
            return res as any;
        } else {
            const elemRes = await this.transformElement(data);
            if (elemRes) {
                res.detections = res.detections.concat(elemRes.detections);
                if (elemRes.temperature) {
                    res.temperatures.push(elemRes.temperature);
                }
            }
            return res as any;
        }
    };

    protected transformElement = async (element: any): Promise<any> => {
        const measuredFrom = DateTime.fromISO(element.datetime).setTimeZone("Europe/Prague");
        const measuredTo = measuredFrom.clone().add(5, "minutes");

        const res = {
            detections: element.directions
                ? element.directions.map((direction: Record<string, any>) => ({
                      directions_id: "camea-" + direction.id,
                      locations_id: "camea-" + element.bikecounter,
                      measured_from: measuredFrom.toISOString(),
                      measured_to: measuredTo.toISOString(),
                      value: direction.detections,
                      value_pedestrians:
                          direction.pedestrians !== undefined && direction.pedestrians !== -1 ? direction.pedestrians : null,
                  }))
                : [],
            temperature: !isNaN(parseFloat(element.temperature))
                ? {
                      locations_id: "camea-" + element.bikecounter,
                      measured_from: measuredFrom.toISOString(),
                      measured_to: measuredTo.toISOString(),
                      value: parseInt(element.temperature, 10),
                  }
                : null,
        };

        return res;
    };
}
