/* ie/repositories/index.ts */
export * from "./DetectionsRepository";
export * from "./DirectionsRepository";
export * from "./LocationsRepository";
export * from "./TemperaturesRepository";
