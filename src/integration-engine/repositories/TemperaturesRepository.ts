import { BicycleCounters } from "#sch";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class TemperaturesRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            BicycleCounters.temperatures.name + "Model",
            {
                outputSequelizeAttributes: BicycleCounters.temperatures.outputSequelizeAttributes,
                pgTableName: BicycleCounters.temperatures.pgTableName,
                pgSchema: BicycleCounters.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                BicycleCounters.temperatures.name + "ModelValidator",
                BicycleCounters.temperatures.outputJsonSchemaObject
            )
        );
    }
}
