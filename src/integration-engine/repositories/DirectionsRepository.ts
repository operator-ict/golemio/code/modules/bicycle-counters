import { BicycleCounters } from "#sch";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class DirectionRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            BicycleCounters.directions.name + "Model",
            {
                outputSequelizeAttributes: BicycleCounters.directions.outputSequelizeAttributes,
                pgTableName: BicycleCounters.directions.pgTableName,
                pgSchema: BicycleCounters.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                BicycleCounters.directions.name + "ModelValidator",
                BicycleCounters.directions.outputJsonSchemaObject
            )
        );
    }
}
