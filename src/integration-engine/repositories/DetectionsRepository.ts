import { BicycleCounters } from "#sch";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class DetectionsRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            BicycleCounters.detections.name + "Model",
            {
                outputSequelizeAttributes: BicycleCounters.detections.outputSequelizeAttributes,
                pgTableName: BicycleCounters.detections.pgTableName,
                pgSchema: BicycleCounters.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                BicycleCounters.detections.name + "ModelValidator",
                BicycleCounters.detections.outputJsonSchemaObject
            )
        );
    }
}
