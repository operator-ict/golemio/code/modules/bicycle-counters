import { BicycleCounters } from "#sch";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class LocationsRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            BicycleCounters.locations.name + "Model",
            {
                outputSequelizeAttributes: BicycleCounters.locations.outputSequelizeAttributes,
                pgTableName: BicycleCounters.locations.pgTableName,
                pgSchema: BicycleCounters.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                BicycleCounters.locations.name + "ModelValidator",
                BicycleCounters.locations.outputJsonSchemaObject
            )
        );
    }
}
