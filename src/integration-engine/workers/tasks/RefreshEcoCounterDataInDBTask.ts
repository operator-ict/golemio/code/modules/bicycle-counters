import { AbstractEmptyTask, DataSource, QueueManager } from "@golemio/core/dist/integration-engine";
import { DirectionRepository, LocationsRepository } from "#ie/repositories";
import { EcoCounterTransformation } from "#ie/transformations/EcoCounterTransformation";
import { EcoCountersDataSource } from "#ie/datasources";

export class RefreshEcoCounterDataInDBTask extends AbstractEmptyTask {
    public readonly queueName = "refreshEcoCounterDataInDB";
    public readonly queueTtl = 14 * 60 * 1000; // 14 minutes

    private dataSourceCamea: DataSource;
    private ecoCounterTransformation: EcoCounterTransformation;
    private locationsRepo: LocationsRepository;
    private directionsRepo: DirectionRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.dataSourceCamea = EcoCountersDataSource.getDataSource();
        this.ecoCounterTransformation = new EcoCounterTransformation();
        this.locationsRepo = new LocationsRepository();
        this.directionsRepo = new DirectionRepository();
    }

    protected async execute(): Promise<void> {
        const data = await this.dataSourceCamea.getAll();

        const transformedData = await this.ecoCounterTransformation.transform(data);

        await this.locationsRepo.save(transformedData.locations);
        await this.directionsRepo.save(transformedData.directions);

        const promises = transformedData.directions.map((p) => {
            return QueueManager.sendMessageToExchange(this.queuePrefix, "updateEcoCounter", {
                category: "bicycle",
                directions_id: p.id,
                id: p.vendor_id,
                locations_id: p.locations_id,
            });
        });
        await Promise.all(promises);
    }
}
