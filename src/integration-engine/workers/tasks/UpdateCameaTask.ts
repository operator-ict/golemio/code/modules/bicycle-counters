import { CameaCountersMeasurementsDataSource } from "#ie/datasources/CameaCountersMeasurementsDataSource";
import { TemperaturesRepository } from "#ie/repositories";
import { CameaMeasurementsTransformation } from "#ie/transformations/CameaMeasurementsTransformation";
import { dateTime } from "@golemio/core/dist/helpers/DateTime";
import { AbstractTask, DataSource, config } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { RecoverableError } from "@golemio/core/dist/shared/golemio-errors";
import { DetectionsRepository } from "../../repositories/DetectionsRepository";
import { CameaRefreshDurations } from "../BicycleCountersWorker";
import { IUpdateCameaDateInput, IUpdateCameaInput, UpdateCameaValidationSchema } from "./../../schema/UpdateCameaSchema";

export class UpdateCameaTask extends AbstractTask<IUpdateCameaInput> {
    protected schema = UpdateCameaValidationSchema;
    public readonly queueName = "updateCamea";
    public readonly queueTtl = 4 * 60 * 1000; // 4 minutes

    private dataSourceCameaMeasurements: DataSource;

    private cameaMeasurementsTransformation: CameaMeasurementsTransformation;

    private detectionsRepo: DetectionsRepository;
    private temperaturesRepo: TemperaturesRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);

        this.detectionsRepo = new DetectionsRepository();

        this.cameaMeasurementsTransformation = new CameaMeasurementsTransformation();
        this.dataSourceCameaMeasurements = CameaCountersMeasurementsDataSource.getDataSource();
        this.temperaturesRepo = new TemperaturesRepository();
    }

    protected async execute(inputData: IUpdateCameaInput & IUpdateCameaDateInput) {
        const id = inputData.id as string;
        const duration = inputData.duration as CameaRefreshDurations;
        const now = dateTime(new Date()).setTimeZone("UTC");
        let from: string;
        let to: string;

        switch (duration) {
            case CameaRefreshDurations.last3Hours:
                const step = 5;
                const remainder = step - (now.toDate().getMinutes() % step);

                const nowRounded = dateTime(new Date(now.toDate().setUTCSeconds(0, 0))).add(remainder, "minutes");
                const nowMinus12h = nowRounded.clone().subtract(3, "hours");

                to = nowRounded.format("yyyy-LL-dd HH:mm:ss");
                from = nowMinus12h.format("yyyy-LL-dd HH:mm:ss");

                break;
            case CameaRefreshDurations.previousDay:
                const todayStart = dateTime(new Date(now.toDate().setUTCHours(0, 0, 0, 0)));
                const yesterdayStart = todayStart.clone().subtract(1, "days");

                to = todayStart.format("yyyy-LL-dd HH:mm:ss");
                from = yesterdayStart.format("yyyy-LL-dd HH:mm:ss");

                break;
            case CameaRefreshDurations.specificDay:
                const date = inputData.date as string;

                const dayStart = dateTime(new Date(new Date(date).setUTCHours(0, 0, 0, 0)));
                const nextDayStart = dayStart.clone().add(1, "days");

                to = nextDayStart.format("yyyy-LL-dd HH:mm:ss");
                from = dayStart.format("yyyy-LL-dd HH:mm:ss");

                break;
            default:
                throw new RecoverableError(`Undefined Camea refresh duration value.`, this.constructor.name);
        }

        let url = config.datasources.BicycleCountersCameaMeasurements;

        url = url.replace(":id", id);
        url = url.replace(":from", from);
        url = url.replace(":to", to);

        this.dataSourceCameaMeasurements.setProtocolStrategy(
            new HTTPFetchProtocolStrategy({
                headers: {},
                responseType: "json",
                method: "GET",
                url,
            })
        );

        const data = await this.dataSourceCameaMeasurements.getAll();
        const transformedData = await this.cameaMeasurementsTransformation.transform(data);

        await this.detectionsRepo.saveBySqlFunction(transformedData.detections, [
            "locations_id",
            "directions_id",
            "measured_from",
        ]);
        await this.temperaturesRepo.saveBySqlFunction(transformedData.temperatures, ["locations_id", "measured_from"]);
    }
}
