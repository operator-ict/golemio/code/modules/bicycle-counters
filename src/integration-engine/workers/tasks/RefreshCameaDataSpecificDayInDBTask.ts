import { DateValidationSchema, IDate } from "./../../schema/UpdateCameaSchema";
import { CameaDataSource } from "../../datasources/CameaDataSource";
import { AbstractTask, DataSource, QueueManager } from "@golemio/core/dist/integration-engine";
import { DirectionRepository, LocationsRepository } from "#ie/repositories";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { CameaTransformation } from "#ie/transformations/CameaTransformation";
import { CameaRefreshDurations } from "../BicycleCountersWorker";

export class RefreshCameaDataSpecificDayInDBTask extends AbstractTask<IDate> {
    protected schema = DateValidationSchema;
    public readonly queueName = "refreshCameaDataSpecificDayInDB";
    public readonly queueTtl = 5 * 60 * 1000; // 5 minutes

    private dataSourceCamea: DataSource;
    private cameaTransformation: CameaTransformation;
    private locationsRepo: LocationsRepository;
    private directionsRepo: DirectionRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.dataSourceCamea = CameaDataSource.getDataSource();
        this.cameaTransformation = new CameaTransformation();
        this.locationsRepo = new LocationsRepository();
        this.directionsRepo = new DirectionRepository();
    }

    protected async execute(inputData: IDate): Promise<void> {
        if (!inputData?.date) {
            throw new GeneralError("Message must contain the date property.", this.constructor.name);
        }

        const data = await this.dataSourceCamea.getAll();
        const transformedData = await this.cameaTransformation.transform(data);
        await this.locationsRepo.save(transformedData.locations);
        await this.directionsRepo.save(transformedData.directions);

        const promises = transformedData.locations.map((p) => {
            return QueueManager.sendMessageToExchange(this.queuePrefix, "updateCamea", {
                date: inputData.date as string,
                duration: CameaRefreshDurations.specificDay,
                id: p.vendor_id,
            });
        });
        await Promise.all(promises);
    }
}
