import { CameaDataSource } from "../../datasources/CameaDataSource";
import { AbstractEmptyTask, DataSource, QueueManager } from "@golemio/core/dist/integration-engine";
import { DirectionRepository, LocationsRepository } from "#ie/repositories";
import { CameaTransformation } from "#ie/transformations/CameaTransformation";
import { CameaRefreshDurations } from "../BicycleCountersWorker";

export class RefreshCameaDataLastXHoursInDBTask extends AbstractEmptyTask {
    public readonly queueName = "refreshCameaDataLastXHoursInDB";
    public readonly queueTtl = 14 * 60 * 1000; // 14 minutes

    private dataSourceCamea: DataSource;
    private cameaTransformation: CameaTransformation;
    private locationsRepo: LocationsRepository;
    private directionsRepo: DirectionRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.dataSourceCamea = CameaDataSource.getDataSource();
        this.cameaTransformation = new CameaTransformation();
        this.locationsRepo = new LocationsRepository();
        this.directionsRepo = new DirectionRepository();
    }

    protected async execute(): Promise<void> {
        const data = await this.dataSourceCamea.getAll();
        const transformedData = await this.cameaTransformation.transform(data);
        await this.locationsRepo.save(transformedData.locations);
        await this.directionsRepo.save(transformedData.directions);

        const promises = transformedData.locations.map((p) => {
            return QueueManager.sendMessageToExchange(this.queuePrefix, "updateCamea", {
                id: p.vendor_id,
                duration: CameaRefreshDurations.last3Hours,
            });
        });
        await Promise.all(promises);
    }
}
