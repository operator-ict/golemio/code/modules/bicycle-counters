import { EcoCounterMeasurementsDataSource } from "#ie/datasources/EcoCounterMeasurementsDataSource";
import { IUpdateEcoCounterInput, UpdateEcoCounterValidationSchema } from "#ie/schema/UpdateEcoCounterSchema";
import { EcoCounterMeasurementsTransformation } from "#ie/transformations/EcoCounterMeasurementsTransformation";
import { dateTime } from "@golemio/core/dist/helpers/DateTime";
import { AbstractTask, config, DataSource } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { DetectionsRepository } from "../../repositories/DetectionsRepository";

export class UpdateEcoCounterTask extends AbstractTask<IUpdateEcoCounterInput> {
    protected schema = UpdateEcoCounterValidationSchema;
    public readonly queueName = "updateEcoCounter";
    public readonly queueTtl = 14 * 60 * 1000; // 14 minutes

    private dataSourceEcoCounterMeasurements: DataSource;

    private ecoCounterMeasurementsTransformation: EcoCounterMeasurementsTransformation;

    private detectionsRepo: DetectionsRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);

        this.dataSourceEcoCounterMeasurements = EcoCounterMeasurementsDataSource.getDataSource();
        this.detectionsRepo = new DetectionsRepository();
        this.ecoCounterMeasurementsTransformation = new EcoCounterMeasurementsTransformation();
    }

    protected async execute(inputData: IUpdateEcoCounterInput) {
        const locationsId = inputData.locations_id;
        const directionsId = inputData.directions_id;
        const category = inputData.category;
        const id = inputData.id;

        // EcoCounter API is actually working with local Europe/Prague time, not ISO!!!
        // so we have to send local time to request.
        // Furthermore, the returned dates are START of the measurement interval, so if we want measurements
        // from interval between 06:00 and 07:00 UTC (which is local 07:00 - 08:00), we have to send parameters
        // from=07:00 and to=07:45, because it returns all the measurements where from and to parameters are INCLUDED.
        const now = new Date();

        const step = 15;

        const remainder = now.getMinutes() % step;

        // rounded to nearest next 15 minutes
        const nowRound = dateTime(now).subtract(remainder, "minutes").toDate().setUTCSeconds(0, 0);

        const strTo = dateTime(new Date(nowRound), { timeZone: "UTC" })
            .setTimeZone("Europe/Prague")
            .subtract(step, "minutes")
            .format("yyyy-LL-dd'T'HH:mm:ss");

        const strFrom = dateTime(new Date(nowRound), { timeZone: "UTC" })
            .setTimeZone("Europe/Prague")
            .subtract(12, "hours")
            .format("yyyy-LL-dd'T'HH:mm:ss");

        let url = config.datasources.BicycleCountersEcoCounterMeasurements;
        url = url.replace(":id", id);
        url = url.replace(":from", strFrom);
        url = url.replace(":to", strTo);
        url = url.replace(":step", `${step}m`);
        url = url.replace(":complete", "true");

        this.dataSourceEcoCounterMeasurements.setProtocolStrategy(
            new HTTPFetchProtocolStrategy({
                headers: {
                    Authorization: `Bearer ${config.datasources.CountersEcoCounterTokens.PRAHA}`,
                },
                responseType: "json",
                method: "GET",
                url,
            })
        );

        const data = await this.dataSourceEcoCounterMeasurements.getAll();

        if (category === "bicycle") {
            await this.detectionsRepo.saveBySqlFunction(
                (
                    await this.ecoCounterMeasurementsTransformation.transform(data)
                ).map((x: any) => {
                    x.directions_id = directionsId;
                    x.locations_id = locationsId;
                    return x;
                }),
                ["locations_id", "directions_id", "measured_from"]
            );
        }
    }
}
