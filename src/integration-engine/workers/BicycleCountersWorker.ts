import { UpdateEcoCounterTask } from "./tasks/UpdateEcoCounterTask";
import { UpdateCameaTask } from "./tasks/UpdateCameaTask";
import { AbstractWorker } from "@golemio/core/dist/integration-engine";
import { RefreshCameaDataSpecificDayInDBTask, RefreshEcoCounterDataInDBTask } from "./tasks";
import { RefreshCameaDataLastXHoursInDBTask } from "./tasks/RefreshCameaDataLastXHoursInDBTask";
import { RefreshCameaDataPreviousDayInDBTask } from "./tasks/RefreshCameaDataPreviousDayInDBTask";

export enum CameaRefreshDurations {
    last3Hours,
    previousDay,
    specificDay,
}

export class BicycleCountersWorker extends AbstractWorker {
    protected readonly name = "BicycleCounters";

    constructor() {
        super();
        this.registerTask(new RefreshCameaDataLastXHoursInDBTask(this.getQueuePrefix()));
        this.registerTask(new RefreshCameaDataPreviousDayInDBTask(this.getQueuePrefix()));
        this.registerTask(new RefreshCameaDataSpecificDayInDBTask(this.getQueuePrefix()));

        this.registerTask(new RefreshEcoCounterDataInDBTask(this.getQueuePrefix()));

        this.registerTask(new UpdateCameaTask(this.getQueuePrefix()));
        this.registerTask(new UpdateEcoCounterTask(this.getQueuePrefix()));
    }
}
