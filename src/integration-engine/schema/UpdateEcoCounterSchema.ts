import { IsString, IsNotEmpty, IsOptional, ValidateIf } from "@golemio/core/dist/shared/class-validator";

export interface IUpdateEcoCounterInput {
    category: string;
    directions_id: string;
    id: string;
    locations_id: string;
}

export class UpdateEcoCounterValidationSchema implements IUpdateEcoCounterInput {
    @IsNotEmpty()
    id!: string;
    @IsString()
    category!: string;
    @IsString()
    directions_id!: string;
    @IsString()
    locations_id!: string;
}
