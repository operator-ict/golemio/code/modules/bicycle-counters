import { IsNumber, IsString, IsOptional } from "@golemio/core/dist/shared/class-validator";

export interface IUpdateCameaInput {
    id: string;
    duration: number;
}

export interface IUpdateCameaDateInput {
    id: string;
    duration: number;
    date?: string;
}

export interface IDate {
    date: string;
}

export class UpdateCameaValidationSchema implements IUpdateCameaInput {
    @IsString()
    id!: string;
    @IsNumber()
    duration!: number;
    @IsOptional()
    @IsString()
    date!: string;
}

export class DateValidationSchema implements IDate {
    @IsString()
    date!: string;
}
