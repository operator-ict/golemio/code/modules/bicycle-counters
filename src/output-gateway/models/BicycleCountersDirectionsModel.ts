import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { BicycleCounters } from "#sch/index";

export class BicycleCountersDirectionsModel extends SequelizeModel {
    public constructor() {
        super(
            BicycleCounters.directions.name,
            BicycleCounters.directions.pgTableName,
            BicycleCounters.directions.outputSequelizeAttributes,
            {
                schema: BicycleCounters.pgSchema,
            }
        );
    }

    public GetAll = async (): Promise<null> => null;

    public GetOne = async (): Promise<null> => null;
}
