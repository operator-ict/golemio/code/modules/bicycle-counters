import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { BicycleCounters } from "#sch/index";
import { IDetection } from ".";

export class BicycleCountersDetectionsModel extends SequelizeModel {
    public constructor() {
        super(
            BicycleCounters.detections.name,
            BicycleCounters.detections.pgTableName,
            BicycleCounters.detections.outputSequelizeAttributes,
            {
                schema: BicycleCounters.pgSchema,
            }
        );

        this.sequelizeModel.removeAttribute("update_batch_id");
        this.sequelizeModel.removeAttribute("create_batch_id");
        this.sequelizeModel.removeAttribute("updated_by");
        this.sequelizeModel.removeAttribute("updated_at");
        this.sequelizeModel.removeAttribute("created_by");
        this.sequelizeModel.removeAttribute("created_at");
    }

    /**
     * @param {object} [options] Options object with params
     * @param {number} [options.limit] Limit
     * @param {number} [options.offset] Offset
     * @param {string} [options.isoDateFrom] ISO8601 string datetime
     * @param {string} [options.isoDateTo] ISO8601 string datetime
     * @param {boolean} [options.aggregate] if not false|undefined sum aggregated by directions_id is returned
     * @param {array} [options.id[]] direction_ids (locations_id? // To discuss)
     * @returns Array of the retrieved records
     */
    public GetAll = async (
        options: {
            limit?: number;
            offset?: number;
            isoDateTo?: any;
            isoDateFrom?: any;
            id?: string[];
            aggregate?: boolean;
        } = {}
    ): Promise<IDetection[]> => {
        const { limit, offset, isoDateFrom, isoDateTo, id, aggregate } = options;
        try {
            const order: string[][] = [["directions_id", "desc"]];
            const attributes: any[] = [["directions_id", "id"]];

            let group: string[] | undefined;
            const where: any = {};

            if (aggregate) {
                attributes.push([Sequelize.fn("sum", Sequelize.col("value")), "value"]);
                attributes.push([Sequelize.fn("sum", Sequelize.col("value_pedestrians")), "value_pedestrians"]);
                attributes.push([Sequelize.literal("STRING_AGG(distinct locations_id,', ')"), "locations_id"]);
                attributes.push([Sequelize.fn("count", Sequelize.col("directions_id")), "measurement_count"]);
                group = ["directions_id"];
            } else {
                attributes.push("value");
                attributes.push("value_pedestrians");
                attributes.push("measured_from");
                attributes.push("measured_to");
                attributes.push("locations_id");
                order.push(["locations_id", "desc"]);
                order.push(["measured_from", "desc"]);
            }

            if (id && Array.isArray(id) && id.length > 0) {
                where.directions_id = id;
            }

            if (isoDateFrom && isoDateTo) {
                where.measured_from = {
                    [Sequelize.Op.between]: [isoDateFrom.getTime(), isoDateTo.getTime()],
                };
            } else {
                if (isoDateFrom) {
                    where.measured_from = {
                        [Sequelize.Op.gte]: isoDateFrom.getTime(),
                    };
                }

                if (isoDateTo) {
                    where.measured_from = {
                        [Sequelize.Op.lte]: isoDateTo.getTime(),
                    };
                }
            }
            return await this.sequelizeModel.findAll({
                attributes,
                group,
                limit,
                offset,
                order: order as any[],
                raw: true, // we should use raw whenever it is possible imho
                where,
            });
        } catch (err) {
            throw new GeneralError("Database error", "BicycleCountersDetectionsModel", err, 500);
        }
    };

    public GetOne = async (): Promise<null> => null;
}
