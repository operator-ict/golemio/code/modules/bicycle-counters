import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { sequelizeConnection } from "@golemio/core/dist/output-gateway/database";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { BicycleCounters } from "#sch/index";
import { IBicycleCountersModels, ILocation } from ".";

export class BicycleCountersLocationsModel extends SequelizeModel {
    public constructor() {
        super(
            BicycleCounters.locations.name,
            BicycleCounters.locations.pgTableName,
            BicycleCounters.locations.outputSequelizeAttributes,
            {
                schema: BicycleCounters.pgSchema,
            }
        );

        this.sequelizeModel.removeAttribute("vendor_id");
        this.sequelizeModel.removeAttribute("update_batch_id");
        this.sequelizeModel.removeAttribute("create_batch_id");
        this.sequelizeModel.removeAttribute("updated_by");
        this.sequelizeModel.removeAttribute("created_at");
    }

    public Associate = (model: IBicycleCountersModels) => {
        this.sequelizeModel.hasMany(model.BicycleCountersDirectionsModel.sequelizeModel, {
            as: "directions",
            foreignKey: "locations_id",
        });
    };

    /**
     * @param {object} [options] Options object with params
     * @param {number} [options.limit] Limit
     * @param {number} [options.offset] Offset
     * @param {number} [options.lat] Latitude to sort results by (by proximity)
     * @param {number} [options.lng] Longitude to sort results by
     * @param {number} [options.range] Maximum range from specified latLng. <br>
     *     Only data within this range will be returned.
     * @returns Array of the retrieved records
     */
    public GetAll = async (
        options: {
            limit?: number;
            offset?: number;
            lat?: number;
            lng?: number;
            range?: number;
        } = {}
    ): Promise<ILocation[]> => {
        const { limit, offset, lat, lng, range } = options;
        try {
            const order: any[] = [];
            const attributes: any[] = ["id", "name", "route", "updated_at", "lng", "lat"];
            const include = [
                {
                    as: "directions",
                    attributes: ["id", "name"],
                    model: sequelizeConnection?.models[BicycleCounters.directions.pgTableName],
                },
            ];
            let where: any = {};
            if (lat && lng) {
                const location = Sequelize.literal(`ST_GeomFromText('POINT(${lng} ${lat})')`);
                const distance = Sequelize.fn("ST_DistanceSphere", Sequelize.literal("ST_MakePoint(lng, lat)"), location);

                attributes.push([distance, "distance"]);
                order.push([Sequelize.literal("distance"), "asc"]);

                if (range) {
                    where = Sequelize.where(distance, "<=", range as any);
                }
            }

            order.push(["id", "asc"]);

            return await this.sequelizeModel.findAll({
                attributes,
                include,
                limit,
                offset,
                order,
                raw: true,
                where,
            });
        } catch (err) {
            throw new GeneralError("Database error", "BicycleCountersLocationsModel", err, 500);
        }
    };

    public GetOne = async (): Promise<null> => null;
}
