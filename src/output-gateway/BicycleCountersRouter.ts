/**
 *
 * Router /WEB LAYER/: maps routes to specific controller functions, passes request parameters and handles responses.
 * Handles web logic (http request, response). Sets response headers, handles error responses.
 */

import {
    IGeoJSONFeatureCollection,
    buildGeojsonFeatureCollection,
    parseCoordinates,
} from "@golemio/core/dist/output-gateway/Geo";
import { checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway/Validation";
import { BaseRouter } from "@golemio/core/dist/output-gateway/routes/BaseRouter";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { query } from "@golemio/core/dist/shared/express-validator";

import { BicycleCountersContainer } from "#og/ioc/Di";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway/CacheHeaderMiddleware";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { IDetection, ILocation, ILocationNormalized, models } from "./models";
import { BicycleCountersDetectionsModel } from "./models/BicycleCountersDetectionsModel";
import { BicycleCountersLocationsModel } from "./models/BicycleCountersLocationsModel";
import { BicycleCountersTemperaturesModel } from "./models/BicycleCountersTemperaturesModel";

export class BicycleCountersRouter extends BaseRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();

    protected BicycleCountersLocationsModel: BicycleCountersLocationsModel;
    protected BicycleCountersDetectionsModel: BicycleCountersDetectionsModel;
    protected BicycleCountersTemperaturesModel: BicycleCountersTemperaturesModel;
    protected cacheHeaderMiddleware: CacheHeaderMiddleware;

    public constructor() {
        super();
        this.cacheHeaderMiddleware = BicycleCountersContainer.resolve<CacheHeaderMiddleware>(
            ContainerToken.CacheHeaderMiddleware
        );

        this.BicycleCountersLocationsModel = models.BicycleCountersLocationsModel;
        this.BicycleCountersDetectionsModel = models.BicycleCountersDetectionsModel;
        this.BicycleCountersTemperaturesModel = models.BicycleCountersTemperaturesModel;
        this.initRoutes();
    }

    public GetAll = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const coords = await parseCoordinates(req.query.latlng as string, req.query.range as string);
            const data = await this.BicycleCountersLocationsModel.GetAll({
                lat: coords.lat,
                limit: Number(req.query.limit) || undefined,
                lng: coords.lng,
                offset: Number(req.query.offset) || 0,
                range: coords.range,
            });

            res.status(200).send(this.NormalizeLocations(data));
        } catch (err) {
            next(err);
        }
    };

    /**
     * get method for get BicycleCountersDetections or BicycleCountersTemperatures depending on given model
     */
    public GetData = (model: BicycleCountersDetectionsModel | BicycleCountersTemperaturesModel) => {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                // to make it a bit foolproof
                const isoDateTo: any = req.query.to ? new Date(req.query.to as string) : null;
                // imho we should limit date range
                const isoDateFrom = req.query.from ? new Date(req.query.from as string) : null;
                const aggregate = (req.query.aggregate as string) === "true";

                const data = await model.GetAll({
                    aggregate,
                    id: this.ConvertToArray(req.query.id || []),
                    isoDateFrom,
                    isoDateTo,
                    limit: Number(req.query.limit) || undefined,
                    offset: Number(req.query.offset) || 0,
                });

                data.forEach((element: IDetection) => {
                    if (aggregate) {
                        element.measured_from = (isoDateFrom || new Date("1970-01-01")).toISOString();
                        element.measured_to = (isoDateTo || new Date()).toISOString();
                        element.value = Math.round(element.value * 100) / 100;
                    } else {
                        element.measurement_count = 1;
                    }
                });

                res.status(200).send(data);
            } catch (err) {
                next(err);
            }
        };
    };

    /**
     * transforms raw DB output ILocation[] to desired IGeoJSONFeatureCollection
     * API output by grouping  directions by locations
     */
    private NormalizeLocations = (locations: ILocation[]): IGeoJSONFeatureCollection => {
        const normalizedData: ILocationNormalized[] = [];
        const indexes: { [key: string]: number } = {};
        locations.forEach((location: ILocation) => {
            if (indexes[location.id] !== undefined) {
                normalizedData[indexes[location.id]].directions.push({
                    id: location["directions.id"],
                    name: location["directions.name"],
                });
            } else {
                indexes[location.id] = normalizedData.length;
                normalizedData.push({
                    directions: [
                        {
                            id: location["directions.id"],
                            name: location["directions.name"],
                        },
                    ],
                    id: location.id,
                    lat: location.lat,
                    lng: location.lng,
                    name: location.name,
                    route: location.route,
                    updated_at: location.updated_at,
                });
            }
        });
        return buildGeojsonFeatureCollection(normalizedData, "lng", "lat", true);
    };

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     * @param {number|string} expire TTL for the caching middleware
     */
    private initRoutes = (expire?: number | string): void => {
        this.router.get(
            "/temperatures",
            [
                query("from").optional().isISO8601().not().isArray(),
                query("to").optional().isISO8601().not().isArray(),
                query("id").optional().not().isEmpty({ ignore_whitespace: true }),
                query("aggregate").optional().isBoolean().not().isArray(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("BicycleCountersRouter"),
            this.cacheHeaderMiddleware.getMiddleware(60 * 60, 300),
            this.GetData(this.BicycleCountersTemperaturesModel)
        );

        this.router.get(
            "/detections",
            [
                query("from").optional().isISO8601().not().isArray(),
                query("to").optional().isISO8601().not().isArray(),
                query("id").optional().not().isEmpty({ ignore_whitespace: true }),
                query("aggregate").optional().isBoolean().not().isArray(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("BicycleCountersRouter"),
            this.cacheHeaderMiddleware.getMiddleware(60 * 60, 300),
            this.GetData(this.BicycleCountersDetectionsModel)
        );

        this.router.get(
            "/",
            [query("latlng").optional().isLatLong().not().isArray(), query("range").optional().isNumeric().not().isArray()],
            pagination,
            checkErrors,
            paginationLimitMiddleware("BicycleCountersRouter"),
            this.cacheHeaderMiddleware.getMiddleware(60 * 60, 300),
            this.GetAll
        );
    };
}

const bicycleCountersRouter: Router = new BicycleCountersRouter().router;

export { bicycleCountersRouter };
