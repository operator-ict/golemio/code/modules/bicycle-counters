import { CameaRefreshDurations } from "#ie/workers/BicycleCountersWorker";
import { RefreshCameaDataPreviousDayInDBTask } from "#ie/workers/tasks";
import { BicycleCounters } from "#sch";
import { config, PostgresConnector, QueueManager } from "@golemio/core/dist/integration-engine";
import sinon from "sinon";
import { SinonSandbox, SinonSpy } from "sinon";

describe("RefreshCameaDataPreviousDayInDBTask", () => {
    let task: RefreshCameaDataPreviousDayInDBTask;
    let sandbox: SinonSandbox;
    let queuePrefix: string;
    let testData: number[];
    let testTransformedData: Record<string, any>;
    let testMeasurementsData: number[];
    let testCameaMeasurementsTransformedData: Record<string, number[]>;
    let testEcoCounterMeasurementsTransformedData: any[];

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub(),
            })
        );

        testData = [1, 2];
        testTransformedData = {
            directions: [
                { vendor_id: "103047647", id: "ecoCounter-103047647", locations_id: "ecoCounter-100047647" },
                { vendor_id: "104047647", id: "ecoCounter-104047647", locations_id: "ecoCounter-100047647" },
            ],
            directionsPedestrians: [
                { vendor_id: "103047647", id: "ecoCounter-103047647", locations_id: "ecoCounter-100047647" },
                { vendor_id: "104047647", id: "ecoCounter-104047647", locations_id: "ecoCounter-100047647" },
            ],
            locations: [{ vendor_id: "BC_BS-BMZL" }, { vendor_id: "BC_AT-STLA" }],
            locationsPedestrians: [{ vendor_id: "BC_BS-BMZL" }, { vendor_id: "BC_AT-STLA" }],
        };
        testMeasurementsData = [1, 2];
        testCameaMeasurementsTransformedData = {
            detections: [1, 2],
            temperatures: [1, 2],
        };
        testEcoCounterMeasurementsTransformedData = [
            {
                directions_id: null,
                locations_id: null,
                measured_from: new Date().toISOString(),
                measured_to: new Date().toISOString(),
                value: 1,
            },
        ];

        task = new RefreshCameaDataPreviousDayInDBTask("test.bicycleCounters");

        sandbox.stub(task["dataSourceCamea"], "getAll").callsFake(() => Promise.resolve(testData));
        sandbox.stub(task["cameaTransformation"], "transform").callsFake(() => Promise.resolve(testTransformedData as any));
        sandbox.stub(task["locationsRepo"], "save");
        sandbox.stub(task["directionsRepo"], "save");
        queuePrefix = config.RABBIT_EXCHANGE_NAME + "." + BicycleCounters.name.toLowerCase();
        sandbox.stub(QueueManager, "sendMessageToExchange" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call correct methods by refreshCameaDataPreviousDayInDB task", async () => {
        await task["execute"]();
        sandbox.assert.calledOnce(task["dataSourceCamea"].getAll as SinonSpy);
        sandbox.assert.calledOnce(task["cameaTransformation"].transform as SinonSpy);
        sandbox.assert.calledWith(task["cameaTransformation"].transform as SinonSpy, testData);
        sandbox.assert.calledOnce(task["locationsRepo"].save as SinonSpy);
        sandbox.assert.calledOnce(task["directionsRepo"].save as SinonSpy);
        sandbox.assert.calledTwice(QueueManager["sendMessageToExchange"] as SinonSpy);

        for (const f of testTransformedData.locations) {
            sandbox.assert.calledWith(QueueManager["sendMessageToExchange"] as SinonSpy, "test.bicycleCounters", "updateCamea", {
                id: f.vendor_id,
                duration: CameaRefreshDurations.previousDay,
            });
        }
        sandbox.assert.callOrder(
            task["dataSourceCamea"].getAll as SinonSpy,
            task["cameaTransformation"].transform as SinonSpy,
            task["locationsRepo"].save as SinonSpy,
            task["directionsRepo"].save as SinonSpy,
            QueueManager["sendMessageToExchange"] as SinonSpy
        );
    });
});
