import { UpdateCameaTask } from "./../../../../src/integration-engine/workers/tasks/UpdateCameaTask";
import { CameaRefreshDurations } from "#ie/workers/BicycleCountersWorker";
import { config, PostgresConnector } from "@golemio/core/dist/integration-engine";
import sinon from "sinon";
import { SinonSandbox, SinonSpy } from "sinon";

describe("UpdateCameaTask", () => {
    let task: UpdateCameaTask;
    let sandbox: SinonSandbox;
    let testData: number[];
    let testMeasurementsData: number[];
    let testCameaMeasurementsTransformedData: Record<string, number[]>;
    let testEcoCounterMeasurementsTransformedData: any[];

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub(),
            })
        );

        testData = [1, 2];

        testMeasurementsData = [1, 2];
        testCameaMeasurementsTransformedData = {
            detections: [1, 2],
            temperatures: [1, 2],
        };
        testEcoCounterMeasurementsTransformedData = [
            {
                directions_id: null,
                locations_id: null,
                measured_from: new Date().toISOString(),
                measured_to: new Date().toISOString(),
                value: 1,
            },
        ];

        task = new UpdateCameaTask("test.bicycleCounters");

        sandbox
            .stub(task["cameaMeasurementsTransformation"], "transform")
            .callsFake(() => Promise.resolve(testCameaMeasurementsTransformedData as any));
        sandbox.stub(task["dataSourceCameaMeasurements"], "getAll").callsFake(() => Promise.resolve(testMeasurementsData));

        sandbox.stub(task["detectionsRepo"], "saveBySqlFunction");
        sandbox.stub(task["temperaturesRepo"], "saveBySqlFunction");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call correct methods by updateCamea task", async () => {
        await task["execute"]({ duration: CameaRefreshDurations.last3Hours, id: "BC_BS-BMZL" });
        sandbox.assert.calledOnce(task["dataSourceCameaMeasurements"].getAll as SinonSpy);
        sandbox.assert.calledOnce(task["cameaMeasurementsTransformation"].transform as SinonSpy);
        sandbox.assert.calledWith(task["cameaMeasurementsTransformation"].transform as SinonSpy, testMeasurementsData);

        sandbox.assert.calledOnce(task["detectionsRepo"].saveBySqlFunction as SinonSpy);
        sandbox.assert.calledOnce(task["temperaturesRepo"].saveBySqlFunction as SinonSpy);

        sandbox.assert.callOrder(
            task["dataSourceCameaMeasurements"].getAll as SinonSpy,
            task["cameaMeasurementsTransformation"].transform as SinonSpy,
            task["detectionsRepo"].saveBySqlFunction as SinonSpy,
            task["temperaturesRepo"].saveBySqlFunction as SinonSpy
        );
    });
});
