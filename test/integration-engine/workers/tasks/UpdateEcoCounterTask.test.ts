import { BicycleCounters } from "#sch";
import { config, PostgresConnector, QueueManager } from "@golemio/core/dist/integration-engine";
import sinon from "sinon";
import { SinonSandbox, SinonSpy } from "sinon";
import { UpdateEcoCounterTask } from "#ie/workers/tasks/UpdateEcoCounterTask";

describe("UpdateEcoCounterTask", () => {
    let task: UpdateEcoCounterTask;
    let sandbox: SinonSandbox;
    let queuePrefix: string;
    let testData: number[];
    let testTransformedData: Record<string, any>;
    let testMeasurementsData: number[];
    let testCameaMeasurementsTransformedData: Record<string, number[]>;
    let testEcoCounterMeasurementsTransformedData: any[];

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub(),
            })
        );

        testData = [1, 2];
        testTransformedData = {
            directions: [
                { vendor_id: "103047647", id: "ecoCounter-103047647", locations_id: "ecoCounter-100047647" },
                { vendor_id: "104047647", id: "ecoCounter-104047647", locations_id: "ecoCounter-100047647" },
            ],
            directionsPedestrians: [
                { vendor_id: "103047647", id: "ecoCounter-103047647", locations_id: "ecoCounter-100047647" },
                { vendor_id: "104047647", id: "ecoCounter-104047647", locations_id: "ecoCounter-100047647" },
            ],
            locations: [{ vendor_id: "BC_BS-BMZL" }, { vendor_id: "BC_AT-STLA" }],
            locationsPedestrians: [{ vendor_id: "BC_BS-BMZL" }, { vendor_id: "BC_AT-STLA" }],
        };
        testMeasurementsData = [1, 2];
        testCameaMeasurementsTransformedData = {
            detections: [1, 2],
            temperatures: [1, 2],
        };
        testEcoCounterMeasurementsTransformedData = [
            {
                directions_id: null,
                locations_id: null,
                measured_from: new Date().toISOString(),
                measured_to: new Date().toISOString(),
                value: 1,
            },
        ];

        task = new UpdateEcoCounterTask("test.bicycleCounters");

        sandbox.stub(task["dataSourceEcoCounterMeasurements"], "getAll").callsFake(() => Promise.resolve(testMeasurementsData));
        sandbox
            .stub(task["ecoCounterMeasurementsTransformation"], "transform")
            .callsFake(() => Promise.resolve(testEcoCounterMeasurementsTransformedData));
        sandbox.stub(task["detectionsRepo"], "save");
        sandbox.stub(task["detectionsRepo"], "saveBySqlFunction");
        queuePrefix = config.RABBIT_EXCHANGE_NAME + "." + BicycleCounters.name.toLowerCase();
        sandbox.stub(QueueManager, "sendMessageToExchange" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call correct methods by UpdateEcoCounter task", async () => {
        await task["execute"]({
            category: "bicycle",
            directions_id: "ecoCounter-103047647",
            id: "103047647",
            locations_id: "ecoCounter-100047647",
        });
        sandbox.assert.calledOnce(task["dataSourceEcoCounterMeasurements"].getAll as SinonSpy);
        sandbox.assert.calledOnce(task["detectionsRepo"].saveBySqlFunction as SinonSpy);
        sandbox.assert.calledOnce(task["ecoCounterMeasurementsTransformation"].transform as SinonSpy);
        sandbox.assert.calledWith(task["ecoCounterMeasurementsTransformation"].transform as SinonSpy, testMeasurementsData);
        sandbox.assert.callOrder(
            task["dataSourceEcoCounterMeasurements"].getAll as SinonSpy,
            task["ecoCounterMeasurementsTransformation"].transform as SinonSpy,
            task["detectionsRepo"].saveBySqlFunction as SinonSpy
        );
    });
});
