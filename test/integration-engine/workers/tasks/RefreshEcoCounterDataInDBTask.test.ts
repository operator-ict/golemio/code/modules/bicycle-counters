import { RefreshEcoCounterDataInDBTask } from "#ie/workers/tasks";
import { BicycleCounters } from "#sch";
import { config, PostgresConnector, QueueManager } from "@golemio/core/dist/integration-engine";
import sinon from "sinon";
import { SinonSandbox, SinonSpy } from "sinon";

describe("RefreshEcoCounterDataInDBTask", () => {
    let task: RefreshEcoCounterDataInDBTask;
    let sandbox: SinonSandbox;
    let queuePrefix: string;
    let testData: number[];
    let testTransformedData: Record<string, any>;
    let testMeasurementsData: number[];
    let testCameaMeasurementsTransformedData: Record<string, number[]>;
    let testEcoCounterMeasurementsTransformedData: any[];

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub(),
            })
        );

        config.datasources = {
            CountersEcoCounterTokens: {
                PRAHA: "",
            },
            BicycleCountersEcoCounterMeasurements: "",
            BicycleCountersCameaMeasurements: "",
            BicycleCountersStatPing: {
                startOffsetSec: 42,
            },
        };

        testData = [1, 2];
        testTransformedData = {
            directions: [
                { vendor_id: "103047647", id: "ecoCounter-103047647", locations_id: "ecoCounter-100047647" },
                { vendor_id: "104047647", id: "ecoCounter-104047647", locations_id: "ecoCounter-100047647" },
            ],
            directionsPedestrians: [
                { vendor_id: "103047647", id: "ecoCounter-103047647", locations_id: "ecoCounter-100047647" },
                { vendor_id: "104047647", id: "ecoCounter-104047647", locations_id: "ecoCounter-100047647" },
            ],
            locations: [{ vendor_id: "BC_BS-BMZL" }, { vendor_id: "BC_AT-STLA" }],
            locationsPedestrians: [{ vendor_id: "BC_BS-BMZL" }, { vendor_id: "BC_AT-STLA" }],
        };
        testMeasurementsData = [1, 2];
        testCameaMeasurementsTransformedData = {
            detections: [1, 2],
            temperatures: [1, 2],
        };
        testEcoCounterMeasurementsTransformedData = [
            {
                directions_id: null,
                locations_id: null,
                measured_from: new Date().toISOString(),
                measured_to: new Date().toISOString(),
                value: 1,
            },
        ];

        task = new RefreshEcoCounterDataInDBTask("test.bicycleCounters");

        sandbox.stub(task["dataSourceCamea"], "getAll").callsFake(() => Promise.resolve(testData));
        sandbox.stub(task["ecoCounterTransformation"], "transform").callsFake(() => Promise.resolve(testTransformedData as any));
        sandbox.stub(task["locationsRepo"], "save");
        sandbox.stub(task["directionsRepo"], "save");
        queuePrefix = config.RABBIT_EXCHANGE_NAME + "." + BicycleCounters.name.toLowerCase();
        sandbox.stub(QueueManager, "sendMessageToExchange" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call correct methods by RefreshEcoCounterDataInDB task", async () => {
        await task["execute"]();
        sandbox.assert.calledOnce(task["dataSourceCamea"].getAll as SinonSpy);
        sandbox.assert.calledOnce(task["ecoCounterTransformation"].transform as SinonSpy);
        sandbox.assert.calledWith(task["ecoCounterTransformation"].transform as SinonSpy, testData);
        sandbox.assert.calledOnce(task["locationsRepo"].save as SinonSpy);
        sandbox.assert.calledOnce(task["directionsRepo"].save as SinonSpy);
        sandbox.assert.calledTwice(QueueManager["sendMessageToExchange"] as SinonSpy);

        testTransformedData.directions.map((f: any) => {
            sandbox.assert.calledWith(
                QueueManager["sendMessageToExchange"] as SinonSpy,
                "test.bicycleCounters",
                "updateEcoCounter",
                {
                    category: "bicycle",
                    directions_id: f.id,
                    id: f.vendor_id,
                    locations_id: f.locations_id,
                }
            );
        });
        sandbox.assert.callOrder(
            task["dataSourceCamea"].getAll as SinonSpy,
            task["ecoCounterTransformation"].transform as SinonSpy,
            task["locationsRepo"].save as SinonSpy,
            task["directionsRepo"].save as SinonSpy,
            QueueManager["sendMessageToExchange"] as SinonSpy
        );
    });
});
