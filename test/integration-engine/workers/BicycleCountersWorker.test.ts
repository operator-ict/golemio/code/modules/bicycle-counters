import { createSandbox, SinonSandbox } from "sinon";
import { expect } from "chai";
import { config, PostgresConnector } from "@golemio/core/dist/integration-engine";
import { BicycleCountersWorker } from "#ie/workers/BicycleCountersWorker";
import { BicycleCounters } from "#sch";

describe("MunicipalLibrariesWorker", () => {
    let sandbox: SinonSandbox;
    let worker: BicycleCountersWorker;
    let queuePrefix: string;

    beforeEach(() => {
        sandbox = createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub(),
            })
        );
        config.datasources = {
            CountersEcoCounterTokens: {
                PRAHA: "",
            },
            BicycleCountersEcoCounterMeasurements: "",
            BicycleCountersCameaMeasurements: "",
            BicycleCountersStatPing: {
                startOffsetSec: 42,
            },
        };
        queuePrefix = config.RABBIT_EXCHANGE_NAME + "." + BicycleCounters.name.toLowerCase();
        worker = new BicycleCountersWorker();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe("getQueuePrefix", () => {
        it("should have correct queue prefix set", () => {
            const result = worker["getQueuePrefix"]();
            expect(result).to.contain(".bicyclecounters");
        });
    });

    describe("getQueueDefinition", () => {
        it("should return correct queue definition", () => {
            const result = worker.getQueueDefinition();
            expect(result.name).to.equal("BicycleCounters");
            expect(result.queues.length).to.equal(6);
        });
    });

    describe("registerTask 1", () => {
        it("should have six tasks in total", () => {
            expect(worker["queues"].length).to.equal(6);
        });
        it("refreshCameaDataLastXHoursInDB registered", () => {
            expect(worker["queues"][0].name).to.equal("refreshCameaDataLastXHoursInDB");
            expect(worker["queues"][0].options.messageTtl).to.equal(14 * 60 * 1000);
        });
    });

    describe("registerTask 2", () => {
        it("refreshCameaDataPreviousDayInDB registered", () => {
            expect(worker["queues"][1].name).to.equal("refreshCameaDataPreviousDayInDB");
            expect(worker["queues"][1].options.messageTtl).to.equal(23 * 60 * 1000);
        });
    });

    describe("registerTask 3", () => {
        it("refreshCameaDataSpecificDayInDB registered", () => {
            expect(worker["queues"][2].name).to.equal("refreshCameaDataSpecificDayInDB");
            expect(worker["queues"][2].options.messageTtl).to.equal(5 * 60 * 1000);
        });
    });

    describe("registerTask 4", () => {
        it("refreshEcoCounterDataInDB registered", () => {
            expect(worker["queues"][3].name).to.equal("refreshEcoCounterDataInDB");
            expect(worker["queues"][3].options.messageTtl).to.equal(14 * 60 * 1000);
        });
    });

    describe("registerTask 5", () => {
        it("updateCamea registered", () => {
            expect(worker["queues"][4].name).to.equal("updateCamea");
            expect(worker["queues"][4].options.messageTtl).to.equal(4 * 60 * 1000);
        });
    });

    describe("registerTask 6", () => {
        it("updateEcoCounter registered", () => {
            expect(worker["queues"][5].name).to.equal("updateEcoCounter");
            expect(worker["queues"][5].options.messageTtl).to.equal(14 * 60 * 1000);
        });
    });
});
