import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { BicycleCounters } from "#sch/index";
import { EcoCounterTransformation } from "#ie/transformations/EcoCounterTransformation";

chai.use(chaiAsPromised);

const readFile = (file: string): Promise<Buffer> => {
    return new Promise((resolve, reject) => {
        const stream = fs.createReadStream(file);
        const chunks: any[] = [];

        stream.on("error", (err) => {
            reject(err);
        });
        stream.on("data", (data) => {
            chunks.push(data);
        });
        stream.on("close", () => {
            resolve(Buffer.concat(chunks));
        });
    });
};

describe("EcoCounterTransformation", () => {
    let transformation: EcoCounterTransformation;
    let testSourceData: any[];
    let locationsValidator: JSONSchemaValidator;
    let directionsValidator: JSONSchemaValidator;

    before(() => {
        locationsValidator = new JSONSchemaValidator(
            BicycleCounters.ecoCounter.name + "LocModelValidator",
            BicycleCounters.locations.outputJsonSchemaObject
        );
        directionsValidator = new JSONSchemaValidator(
            BicycleCounters.ecoCounter.name + "DirModelValidator",
            BicycleCounters.directions.outputJsonSchemaObject
        );
    });

    beforeEach(async () => {
        transformation = new EcoCounterTransformation();
        const buffer = await readFile(__dirname + "/data/bicyclecounters-ecocounter-datasource.json");
        testSourceData = JSON.parse(Buffer.from(buffer).toString("utf8"));
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("EcoCounterBicycleCounters");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform element", async () => {
        const data = await transformation.transform(testSourceData[0]);
        expect(data).to.have.property("directions");
        expect(data).to.have.property("locations");
    });

    it("should properly transform collection", async () => {
        const data = await transformation.transform(testSourceData);

        await expect(locationsValidator.Validate(data.locations)).to.be.fulfilled;
        await expect(directionsValidator.Validate(data.directions)).to.be.fulfilled;

        expect(data).to.have.property("directions");
        expect(data).to.have.property("locations");
    });
});
